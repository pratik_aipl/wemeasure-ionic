// auth-guard.service.ts

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  //   jwtHelper: JwtHelper = new JwtHelper();
  constructor(private router: Router) { }

  canActivate() {
    if (this.loggedIn()) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

  loggedIn() {
    // console.log('isLogin => ',localStorage.getItem('we-user'))
    if (localStorage.getItem('we-user') === null || localStorage.getItem('we-user') === '' ) {
      return false;
    }
    return true;
  }
}
