import { NgModule } from '@angular/core';
import { BrowserModule,HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import {IonicGestureConfig} from "./gestures/ionic-gesture-config";
import { RouteReuseStrategy } from '@angular/router';
import { Network } from '@ionic-native/network/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from 'src/shared/authguard.service';
import { EventService } from 'src/shared/EventService';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Camera } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { ProductListComponent } from './dashboard/order/select-product/product-list/product-list.component';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Device } from '@ionic-native/device/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';


@NgModule({
  declarations: [AppComponent,ProductListComponent],
  entryComponents: [ProductListComponent],
  imports: [BrowserModule, IonicModule.forRoot({
    mode: 'md',
    // scrollAssist: false

    backButtonText: 'Retour',
  // mode: 'ios',
  scrollPadding: false,
  scrollAssist: false
  }),  HttpClientModule, AppRoutingModule],
  providers: [
    StatusBar,Camera,
    File,OneSignal,
    SplashScreen, ImagePicker,PhotoViewer,Device,
     AuthGuard,  File,FileOpener,    EventService,  Network, 
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HAMMER_GESTURE_CONFIG,useClass: IonicGestureConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
