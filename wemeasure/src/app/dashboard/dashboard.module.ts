import { SharedModule } from 'src/shared/shared.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardPage } from './dashboard.page';


const routes: Routes = [
  {
    path: "",
    component: DashboardPage,
  
  }
];

@NgModule({
  entryComponents: [],

  imports: [
    SharedModule, 
    RouterModule.forChild(routes)
  ],
  providers: [
    
  ],
  declarations: [
    DashboardPage,
    ]
})
// @NgModule({
//   imports: [
//    SharedModule,
//     RouterModule.forChild([
//       {
//         path: '',
//         component: HomePage
//       }
//     ])
//   ],
//   declarations: [HomePage]
// })
export class DashboardPageModule {}
