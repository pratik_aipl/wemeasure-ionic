import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { AlertController, IonSlides, NavController } from '@ionic/angular';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
@Component({
  selector: 'app-prodcut-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {
  prodItems:any;
  page: any;
  isShowPrew= false;
  isShowNext= true;
  showMoreImg=false;
  selImage:any='';

  slideOptions = {
    initialSlide: 0,
    // loop: true,
    // autoplay:false,
    pager: false,
  };
  slideOptionsImg = {
    initialSlide: 0,
    // loop: true,
    // autoplay:false,
    pager: false,
  };

  @ViewChild('slider', { static: false }) slider: IonSlides;
  constructor(public activatedRoute: ActivatedRoute, public photoViewer: PhotoViewer,public alertController: AlertController, public router: Router, private apiServices: ApiService, public tools: Tools) {
    this.page = this.activatedRoute.snapshot.paramMap.get('page')
    this.prodItems=this.apiServices.getProductData();
  }

  showImages(){
    this.showMoreImg = !this.showMoreImg
  }

  
  ionViewDidEnter() {
    this.prodItems=this.apiServices.getProductData();
  } 
  ionSlideDidChange(){
    this.slider.getActiveIndex().then(index => {
      console.log(index);
      console.log('currentIndex:', index);
      console.log('length: ', (this.prodItems.Image.length-1));
      if(index == (this.prodItems.Image.length-1)){
        this.isShowNext=false
        this.isShowPrew=true
      }else  if(index == 0){
        this.isShowNext=true
        this.isShowPrew=false
      } else{
        this.isShowNext=true
        this.isShowPrew=true
      }
	  // this.slider.slideNext();
      // OR this.slides.slideTo(index + 1);
    });
  }
  showImage(img){
    // this.showMoreImg = !this.showMoreImg
    // this.selImage=img
    this.photoViewer.show(img);
  }

  slidesDidLoad(slides: IonSlides) {
   // slides.startAutoplay();
  }

  next() {
    this.slider.getActiveIndex().then(index => {
      console.log(index);
      console.log('currentIndex:', index);
      console.log('length: ', (this.prodItems.Image.length-1));
      if(index == (this.prodItems.Image.length-2)){
        this.isShowNext=false
      }else{
        this.isShowNext=true
      }
      this.isShowPrew=true
	  // this.slider.slideNext();
      // OR this.slides.slideTo(index + 1);
    });
    this.slider.slideNext();
  }

  prev() {
    this.slider.getActiveIndex().then(index => {
      console.log(index);
      console.log('currentIndex:', index);
      if(index == 1){
        this.isShowPrew=false
      }else{
        this.isShowPrew=true
      }
      this.isShowNext=true
    });
    this.slider.slidePrev();
  }
  ngOnInit() {
  }

  
  edit() {
    this.router.navigateByUrl('/add-product');
  }
 async delete(item){
    const alert = await this.alertController.create({
      message: 'Are you sure you want to remove Product?',
      buttons: [
          {
              text:  'Cancel',
              role: 'cancel',
              handler: () => {
                
              }
          },
          {
              text:'Remove',
              handler: () => {
                this.removeProduct(item);
              }
          }
      ], backdropDismiss: true
  });
  return await alert.present();
  }
  removeProduct(item){
    // this.navCtrl.back();
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.removeProduct(item.ProductID).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        if (res.status) {
          this.tools.backPage();
        }else{
          this.tools.presentAlert('', res.message, 'Ok');
        }        
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }

  fav(item,status){
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.productFav(item.ProductID,status).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        this.tools.presentAlert('', res.message, 'Ok');
        if (res.status) {
          this.prodItems.isFav=status;
        }
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }
}
