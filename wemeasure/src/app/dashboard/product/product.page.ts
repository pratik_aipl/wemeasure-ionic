import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-prodcut',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
  prodItems = [];
  prodItemsAll = [];
  isSearch=false;
  page: any;
  pageTitle: any;
  message: any;
  catId: any;
  constructor(public activatedRoute: ActivatedRoute, public alertController: AlertController,
     public router: Router, private apiServices: ApiService, public tools: Tools) {
    this.page = this.activatedRoute.snapshot.paramMap.get('page');
    this.pageTitle = this.activatedRoute.snapshot.paramMap.get('page') == 'menu'?'Products':this.activatedRoute.snapshot.paramMap.get('catName')
    this.catId = this.activatedRoute.snapshot.paramMap.get('catId')
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.productCall(this.catId);
  }

  search(){
    this.isSearch= !this.isSearch;
  }
  ionCancel(){
    this.prodItems = [];
    this.prodItems = this.prodItemsAll;
  }
  async ionChange(evt){
    this.prodItems =await this.prodItemsAll;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }
    console.log('Search ',searchTerm);
    this.prodItems = this.prodItems.filter(item => {
      if (item.ProName) {
        return ((item.ProName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1));
      }
    });
  }


  viewProduct(item){
    this.apiServices.setProductData(item);
    this.router.navigateByUrl('/product-details');
  }
  AddProduct() {
    this.router.navigateByUrl('/add-product');
  }
 async delete(item){
    const alert = await this.alertController.create({
      message: 'Are you sure you want to remove Product?',
      buttons: [
          {
              text:  'Cancel',
              role: 'cancel',
              handler: () => {
                
              }
          },
          {
              text:'Remove',
              handler: () => {
                this.removeProduct(item);
              }
          }
      ], backdropDismiss: true
  });
  return await alert.present();
  }

  removeProduct(item){
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.removeProduct(item.ProductID).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        this.tools.presentAlert('', res.message, 'Ok');
        if (res.status) {
          let index: number = this.prodItems.indexOf(item);
          if(index > -1){
            this.prodItems.splice(index, 1);
            }
        }
        
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }

  productCall(catId) {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.getProcudt(catId).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        if (res.status && res.data !=undefined) {
        this.prodItems = res.data.product_list;
        this.prodItemsAll = res.data.product_list;
        }else{
          this.message=res.message;
          this.prodItems = [];
          this.prodItemsAll = [];
        }
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }

  addtowishlist(item,status){
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.productFav(item.ProductID,status).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;

        this.tools.presentAlert('', res.message, 'Ok');
        if (res.status) {
          let index: number = this.prodItems.indexOf(item);
            this.prodItems[index].isFav=status;  
        }
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }
  fav(item,status){
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.productLike(item.ProductID,status).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;

        this.tools.presentAlert('', res.message, 'Ok');
        if (res.status) {
          let index: number = this.prodItems.indexOf(item);
            this.prodItems[index].IsLike=status;  
        }
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }
}
