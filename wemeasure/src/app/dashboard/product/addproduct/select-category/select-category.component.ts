import { ApiService } from 'src/app/services/api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, IonSlides } from '@ionic/angular';
import { Tools } from 'src/shared/tools';
import { Router } from '@angular/router';
@Component({
  selector: 'app-select-category',
  templateUrl: './select-category.component.html',
  styleUrls: ['./select-category.component.scss'],
})
export class SelectCategoryComponent implements OnInit {
  type: any;
  catItems = [];
  constructor(public navParams: NavParams,public router: Router,private apiService:ApiService, public tools: Tools,public modalCtrl: ModalController) {
    this.type = this.navParams.get('value');
    console.log(' ==> ' + this.type)

    // let no=1;
    // for (let i = 0; i < 50; i++) {
    //   this.items.push({CName:'Category Name '+(i+1),CImg:'../../../assets/images/img'+(no)})
    //   no=no+1;
    //   if(no==6)
    //   no=1;
    // }
    
  }
  ngOnInit() { 
    this.categoryCall()
  }

  addCategory(){
    this.modalCtrl.dismiss('new');   
  }
  selectCategory(item){
    this.modalCtrl.dismiss(item);   
  }
  dismissModal() {
    // environment.isBeaconModalOpen = false;
      this.modalCtrl.dismiss('');
  }

  cancel(){
    this.modalCtrl.dismiss('');
  }

  categoryCall() {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiService.getCat().subscribe(response => {
        this.tools.closeLoader();
        let res:any = response;
        

        if (res.status && res.data !=undefined) {
          this.catItems = res.data.category_list;
        } else {
          this.catItems=[];
         }

        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  

      });
    }else{
      this.tools.closeLoader();
    }
  }
}
