import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { ModalController } from '@ionic/angular';
import { SelectCategoryComponent } from './select-category/select-category.component';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';


@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.page.html',
  styleUrls: ['./addproduct.page.scss'],
})
export class AddProductPage implements OnInit {
  pageTitle:any;
  prodItems:any;
  CatID='';
  isShow=false;
  image = [];
  deletedImageID = [];
  imageData = [];

  options = {
    maximumImagesCount: 10,
    width: 200,
    quality: 25,//int (0-100),
    outputType: 1
};


  selectedCat = 'Select Category';
  btnTitle = 'ADD';
  form: FormGroup;
  constructor(public modalController: ModalController,  private imagePicker: ImagePicker,
    public router: Router, public formBuilder: FormBuilder,
    private apiServices: ApiService, public tools: Tools, private camera: Camera,
    public actionSheetController: ActionSheetController,private file: File) {
    this.prodItems=this.apiServices.getProductData();
    
    if(this.prodItems !=undefined){
      this.pageTitle = this.prodItems.ProName;
      for (let i = 0; i < this.prodItems.Image.length; i++) {
        this.image.push({ImageID:this.prodItems.Image[i].ImageID, Image:this.prodItems.Image[i].Image  });
      }
      this.isShow=true;
    }else{
      this.pageTitle='Add Product';
      this.isShow=true;
    }

    this.selectedCat =this.prodItems != undefined?this.prodItems.CategoryName:'Select Category'
    this.CatID =this.prodItems != undefined?this.prodItems.CatID:''
    this.btnTitle =this.prodItems != undefined?'UPDATE':'ADD'

    this.form = this.formBuilder.group({
      ProName: [this.prodItems != undefined?this.prodItems.ProName: '', [Validators.required, Validators.minLength(3), Validators.maxLength(50),Validators.pattern('[a-zA-Z_ ]*$')]],
    });
  }

  ngOnInit() {
    //  this.callStateList()
  }
  async selectCategory() {
    console.log('Select Category Click')
    const modal = await this.modalController.create({
      component: SelectCategoryComponent,
      cssClass: 'modal',
      componentProps: { value: this.prodItems != undefined?this.prodItems.CategoryName:'type' }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        console.log('Selected Cat--> ' + data.data);
        if (data.data) {
          // this.answer.answers[this.currquekey].file = data.data;
          if (data.data == 'new')
            this.router.navigateByUrl('/add-category/new');
          else {
            // this.router.navigateByUrl('/add-category/new');
            console.log('Selected Data ',data);
            this.selectedCat = data.data.Name
            this.CatID=data.data.CategoryID
          }
        }
      });
    // else{
    //   this.tools.presentLogout('Login require','Login','Cancel');
    // }
  }



  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.imageData.push(imageData);
      // this.image.push((<any>window).Ionic.WebView.convertFileSrc(imageData));
      this.image.push({ImageID:'0', Image: 'data:image/png;base64,'+imageData });
    }, (err) => {
      // Handle error
    });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          // this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          this.UploadFile();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  UploadFile() {
    // this.isCamera = false;
    // this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageData.push(results[i]);
        this.image.push({ImageID:'0', Image: 'data:image/png;base64,'+results[i] });
     }
    }, (err) => {
      console.log(' Gallary Pick Error ',err)
     });
  }
  removeImage(pos, item) {
   
    if (item.ImageID) {
      this.deletedImageID.push(item.ImageID);
    }
    console.log('Image  deletedImageID ',  this.deletedImageID);
    let index: number = this.image.indexOf(item);
    if (index > -1) {
      this.image.splice(index, 1);
      this.imageData.splice(index, 1);
    }
  }

  onSubmit(){
    let ProName = this.form.get('ProName').value;   
   
    var msg = ''
    if (this.form.get('ProName').invalid) {       
      if (ProName == '' ) {
        msg = msg + 'Please enter product name<br />'
      }else{
          msg = msg + 'Product name should contain atleast 3 letters<br />'
        }
      }
    if (this.CatID == '') {
      msg = msg + 'Please select category<br />'
    }
    
        if (this.image.length ==0 ){       
          msg = msg + 'Please select at least one image<br />'
        }
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
      if (this.tools.isNetwork()) {
        this.tools.openLoader();
        // Replace extension according to your media type
        // call method that creates a blob from dataUri
        let postData = new FormData();
        // postData.append('file', imageFile);
        if(this.prodItems != undefined)
        postData.append("ProductID", this.prodItems.ProductID);
        postData.append("ProName", ProName);
        postData.append("CatID", this.CatID);
        // postData.append("OrderName", OrderName);
        // postData.append("price", price);
        // postData.append("ProDecription", ProDecription);
        if(this.deletedImageID.length>0){
          postData.append("DeleteImage", Array.prototype.map.call(this.deletedImageID, function (item) { return item; }).join(","));
        }

  
        if(this.imageData.length> 0){
          for (let i = 0; i < this.imageData.length; i++) {
            // const element = array[i];
            const date = new Date().valueOf();
            const imageName = date + '.jpeg';
            var imageBlob:any;
            if(this.imageData.length> 0 )
            imageBlob = this.apiServices.dataURItoBlob(this.imageData[i]);
            postData.append('Image[]', imageBlob, imageName);
          }
          
        }
  
        this.apiServices.addProduct(postData).subscribe(response => {
          this.tools.closeLoader();
          let res:any = response;
  
          if (res.status) {
            this.form.reset();
            this.CatID='';
            this.selectedCat = 'Select Category';
           if(this.prodItems !=undefined) {
             this.apiServices.setProductData(res.data.product_list[0]);
             this.tools.presentAlert('', res.message, 'Ok');
             this.tools.backPage();
           }else{
            this.tools.presentAlert('', res.message, 'Ok');
           }
          } else {
           this.tools.presentAlert('', res.message, 'Ok');
         }
         this.image=[];
         this.imageData=[];
          this.form.reset();
        }, (error: Response) => {
          this.tools.closeLoader();
          this.tools.closeLoader();
          console.log('Error ', error);
          let err:any = error;
          this.tools.openAlertToken(err.status, err.error.message);
    
        });
      }else{
        this.tools.closeLoader();
      }  
    }
    
 }
}
