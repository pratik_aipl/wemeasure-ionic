import { NgModule } from '@angular/core';



import { AddProductPage } from './addproduct.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { SelectCategoryComponent } from './select-category/select-category.component';
const routes: Routes = [
  {
    path: '',
    component: AddProductPage
  }
];

@NgModule({
  entryComponents: [SelectCategoryComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddProductPage,SelectCategoryComponent],
})
export class AddProductPageModule {}
