import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.page.html',
  styleUrls: ['./customer.page.scss'],
})
export class CustomerPage implements OnInit {

  customerItems: any = [];
  customerItemsAll: any = [];
  isSearch=false;
  constructor(public router: Router, public alertController: AlertController, private apiServices: ApiService, public tools: Tools) {
  }


  ngOnInit() {
    //  this.callStateList()
  }


  ionViewDidEnter() {
    this.CustomerCall();
  }

  search(){
    this.isSearch= !this.isSearch;
  }
  ionCancel(){
    this.customerItems = [];
    this.customerItems = this.customerItemsAll;
  }
  async ionChange(evt){
    this.customerItems =await this.customerItemsAll;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }
    console.log('Search ',searchTerm);
    this.customerItems = this.customerItems.filter(item => {
      if (item.FName) {
        return ((item.FName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) || 
        (item.LName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)  );
      }
    });
  }
  cudtomerDetails(item) {
     this.apiServices.setCustomerData(item);
    this.router.navigateByUrl('/customer-details/' + item.FName+' '+item.LName);
  }
  CustomerCall() {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.getCustomer().subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        if(res.data.customer_list != undefined){
          this.customerItems = res.data.customer_list;
          this.customerItemsAll = res.data.customer_list;
        }
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }

  async deleteCustomer(item){
    const alert = await this.alertController.create({
      message: 'Are you sure you want to remove customer?',
      buttons: [
          {
              text:  'Cancel',
              role: 'cancel',
              handler: () => {
                
              }
          },
          {
              text:'Remove',
              handler: () => {
                this.removeCustomer(item);
              }
          }
      ], backdropDismiss: true
  });
  return await alert.present();
  }
  removeCustomer(item) {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.removeCustomer(item.CustomerID).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        this.tools.presentAlert('', res.message, 'Ok');
        if (res.status) {
          let index: number = this.customerItems.indexOf(item);
          if(index > -1){
            this.customerItems.splice(index, 1);
            this.customerItemsAll.splice(index, 1);
            }
        }
        
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }
}
