import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.page.html',
  styleUrls: ['./customer-details.page.scss'],
})
export class CustomerDetailsPage implements OnInit {
 
  items:any;
  name:any;
  constructor(public router: Router,  private activatedRoute: ActivatedRoute, 
    public alertController: AlertController,private apiServices: ApiService,public tools: Tools) { 
    this.name = this.activatedRoute.snapshot.paramMap.get('name')
        this.items=this.apiServices.getCustomerData();    
      }
 
  ngOnInit() {
  //  this.callStateList()
  }

  addOrder(){
    console.log('Add Customer Click');
    // this.apiServices.setCustomerData(this.items);
    this.router.navigateByUrl('/add-customer/order');
  }
  async deleteCustomer(){
    const alert = await this.alertController.create({
      message: 'Are you sure you want to remove customer?',
      buttons: [
          {
              text:  'Cancel',
              role: 'cancel',
              handler: () => {
                
              }
          },
          {
              text:'Remove',
              handler: () => {
                this.removeCustomer();
              }
          }
      ], backdropDismiss: true
  });
  return await alert.present();
  }

  removeCustomer() {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.removeCustomer(this.items.CustomerID).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        this.tools.presentAlert('', res.message, 'Ok');
        if (res.status) {
          this.router.navigateByUrl('/');
        }
        
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }

}
