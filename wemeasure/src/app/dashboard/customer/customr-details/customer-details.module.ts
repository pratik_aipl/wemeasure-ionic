import { NgModule } from '@angular/core';



import { CustomerDetailsPage } from './customer-details.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: CustomerDetailsPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CustomerDetailsPage]
})
export class CustomerDetailsPageModule {}
