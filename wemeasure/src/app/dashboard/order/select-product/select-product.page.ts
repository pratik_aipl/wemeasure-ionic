import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { ModalController } from '@ionic/angular';
import { ProductListComponent } from './product-list/product-list.component';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ImagePicker } from '@ionic-native/image-picker/ngx';

@Component({
  selector: 'app-select-product',
  templateUrl: './select-product.page.html',
  styleUrls: ['./select-product.page.scss'],
})
export class SelectProductPage implements OnInit {
  selectedProd = 'Select Product';
  MeasurementID: any;
  selProdusts = [];

  isCamera = false;

  imageFile = [];
  imageDataFile = [];
  imageCamera = [];
  imageDataCamera = [];

  form: FormGroup;

  Date = '';
  myDate: String = new Date().toISOString();

  options = {
    // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
    // selection of a single image, the plugin will return it.
    maximumImagesCount: 10,
    
    // max width and height to allow the images to be.  Will keep aspect
    // ratio no matter what.  So if both are 800, the returned image
    // will be at most 800 pixels wide and 800 pixels tall.  If the width is
    // 800 and height 0 the image will be 800 pixels wide if the source
    // is at least that wide.
    width: 200,
    // height: 675,
    
    // quality of resized image, defaults to 100
    quality: 25,//int (0-100),

    // output type, defaults to FILE_URIs.
    // available options are 
    // window.imagePicker.OutputType.FILE_URI (0) or 
    // window.imagePicker.OutputType.BASE64_STRING (1)
    outputType: 1
};

  constructor(public router: Router, private activatedRoute: ActivatedRoute, private imagePicker: ImagePicker,
    private camera: Camera, public modalController: ModalController, private apiServices: ApiService,
    public tools: Tools, public formBuilder: FormBuilder) {

    this.MeasurementID = this.activatedRoute.snapshot.paramMap.get('measurementID')
    this.form = this.formBuilder.group({
      OrderName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      price: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(5), Validators.pattern('[0-9]+')]],
      ProDecription: ['', [Validators.maxLength(200)]],
    });

  }

  ngOnInit() {
    //  this.callStateList()
  }

  async selectProduct() {
    console.log('Select Category Click')
    const modal = await this.modalController.create({
      component: ProductListComponent,
      cssClass: 'modal',
      componentProps: { value: 'type' }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        console.log('Selected Cat--> ' + data.data);
        if (data.data) {
          // this.answer.answers[this.currquekey].file = data.data;
          if (data.data == 'new') {
            this.apiServices.setProductData(null);
            this.router.navigateByUrl('/add-product');        
          } else {
            console.log('Selected Data ', data);
            this.selProdusts = data.data
          }
        }
      });
    // else{
    //   this.tools.presentLogout('Login require','Login','Cancel');
    // }
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      if (this.isCamera) {
        this.imageDataCamera.push(imageData);
        this.imageCamera.push('data:image/png;base64,' + imageData);
      } else {
        this.imageDataFile.push(imageData);
        this.imageFile.push('data:image/png;base64,' + imageData);
      }
    }, (err) => {
      // Handle error
    });
  }

  UploadPhoto() {
    this.isCamera = true;
    this.pickImage(this.camera.PictureSourceType.CAMERA);
  }
  UploadFile() {
    this.isCamera = false;
    // this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
    this.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
          this.imageDataFile.push(results[i]);
          this.imageFile.push('data:image/png;base64,' + results[i]);          
      }
    }, (err) => {
      console.log(' Gallary Pick Error ',err)
     });
  }
  removeImage(type, item) {

    if (type == 'prod') {
      let index: number = this.selProdusts.indexOf(item);
      if (index > -1) {
        console.log('Seleted Image Remove ')
        this.selProdusts.splice(index, 1);
      }

    } else if (type == 'file') {
      let index: number = this.imageFile.indexOf(item);
      if (index > -1) {
        console.log('Seleted Image Remove ')
        this.imageFile.splice(index, 1);
        this.imageDataFile.splice(index, 1);
      }

    } else {
      let index: number = this.imageCamera.indexOf(item);
      if (index > -1) {
        console.log('Seleted Image Remove ')
        this.imageCamera.splice(index, 1);
        this.imageDataCamera.splice(index, 1);
      }
    }

  }


  Next() {

    let OrderName = this.form.get('OrderName').value;
    let ProDecription = this.form.get('ProDecription').value;
    let price = this.form.get('price').value;

    var msg = ''
    if (this.form.get('OrderName').invalid) {
      if (name == '') {
        msg = msg + 'Please enter order name<br />'
      } else {
        msg = msg + 'Order name should contain atleast 3 letters <br />'
      }
    }
    if (this.form.get('price').invalid) {
      msg = msg + 'Please enter price <br />'
    }
    console.log('Date ',(this.Date == ''));
    if (this.Date == '') {
      msg = msg + 'Please select delivery date <br />'
    }
    if (this.form.get('ProDecription').invalid) {
      msg = msg + 'Description maximum 200 characters allowed<br />'
    }

    if(this.selProdusts.length == 0 && this.imageDataCamera.length == 0 && this.imageDataFile.length == 0 ){
      msg = msg + 'Please select at least one product or upload photo or upload file<br />'
    }

    if (msg != '') {
      this.tools.openAlert(msg);
    } else {

      if (this.tools.isNetwork()) {
        this.tools.openLoader();
        // Replace extension according to your media type
        // call method that creates a blob from dataUri
        let postData = new FormData();
        // postData.append('file', imageFile);
        postData.append("CustomerID", this.apiServices.getCustomerData().CustomerID);
        postData.append("OrderName", OrderName);
        postData.append("price", price);
        postData.append("ProDecription", ProDecription);
        postData.append("DeliveryDate", this.Date);

        if (this.selProdusts.length > 0)
          postData.append("ProductID", Array.prototype.map.call(this.selProdusts, function (item) { return item.ProductID; }).join(","));

        postData.append("MeasurementID", this.MeasurementID);


        if (this.imageDataCamera.length > 0) {
          for (let i = 0; i < this.imageDataCamera.length; i++) {
            // const element = array[i];
            const date = new Date().valueOf();
            const imageName = date + '.jpeg';
            var imageBlob: any;
            if (this.imageDataCamera.length > 0)
              imageBlob = this.apiServices.dataURItoBlob(this.imageDataCamera[i]);
            postData.append('Image[]', imageBlob, imageName);
          }
        }
        if (this.imageDataFile.length > 0) {
          for (let i = 0; i < this.imageDataFile.length; i++) {
            const date = new Date().valueOf();
            const imageName = date + '.jpeg';
            var imageBlob: any;
            if (this.imageDataFile.length > 0)
              imageBlob = this.apiServices.dataURItoBlob(this.imageDataFile[i]);
            postData.append('Image[]', imageBlob, imageName);
          }
        }
        this.apiServices.addOrder(postData).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;
          if (res.status) {
            this.tools.presentAlert('', res.message, 'Ok');
            this.router.navigateByUrl('/order/order');
            this.imageDataFile=[];
            this.imageFile=[];  
          }else{
            this.tools.presentAlert('', res.message, 'Ok');
          }
        }, (error: Response) => {
          this.tools.closeLoader();
          this.tools.closeLoader();
          console.log('Error ', error);
          let err:any = error;
          this.tools.openAlertToken(err.status, err.error.message);
    
        });
      } else {
        this.tools.closeLoader();
      }

    }
  }
}
