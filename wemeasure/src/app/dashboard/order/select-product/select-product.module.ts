import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';



import { SelectProductPage } from './select-product.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: SelectProductPage
  }
];

@NgModule({

  imports: [
    SharedModule,  ReactiveFormsModule,
    FormsModule   ,
    RouterModule.forChild(routes)
  ],
  declarations: [SelectProductPage]
})
export class SelectProductPageModule {}
