import { ApiService } from 'src/app/services/api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, IonSlides } from '@ionic/angular';
import { Tools } from 'src/shared/tools';
import { Router } from '@angular/router';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  type: any;
  prodItems = [];
  constructor(public navParams: NavParams,public router: Router,private apiService:ApiService, public tools: Tools,public modalCtrl: ModalController) {
    this.type = this.navParams.get('value');
    console.log(' ==> ' + this.type)
    
  }
  ngOnInit() { 
    this.ProductCall()
  }

  AddProduct(){
    this.modalCtrl.dismiss('new');   
  }
  itemChange(event,item){
    console.log('Check Box 0 ',item);
    let index: number = this.prodItems.indexOf(item);
    this.prodItems[index].isCheck=event.detail.checked;  
    console.log('Check Box 1 ',this.prodItems[index]);
  }
  selectProduct(){
    var locList=[];
    for (let i = 0; i < this.prodItems.length; i++) {
      const element = this.prodItems[i];   
      if(element.isCheck){
        locList.push(element);   
      }
    }
    console.log();
  this.modalCtrl.dismiss(locList);   
  }
  dismissModal() {
    // environment.isBeaconModalOpen = false;
      this.modalCtrl.dismiss('');
  }

  cancel(){
    this.modalCtrl.dismiss('');
  }

  ProductCall() {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiService.getProcudt('0').subscribe(response => {
        this.tools.closeLoader();
        let res:any = response;      
        if (res.status) {
          for (let index = 0; index < res.data.product_list.length; index++) {
            const element = res.data.product_list[index];
            element.isCheck=false;
            this.prodItems.push(element)
          }
          // this.prodItems = res.data.product_list;
        } else {
          this.prodItems=[];
         }

        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    }else{
      this.tools.closeLoader();
    }
  }
}
