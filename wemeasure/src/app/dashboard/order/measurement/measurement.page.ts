import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-measurement',
  templateUrl: './measurement.page.html',
  styleUrls: ['./measurement.page.scss'],
})
export class MeasurementPage implements OnInit {
  form: FormGroup;
  page: any;
  Measurement: any;
  isShow=true;
  maxDate: String = new Date().toISOString();
  SleevesTypeNm:any;
  WaitsTypeNm:any;
  DressLengthTypeNm:any;
  TopLengthNm:any;
  BottomLengthNm:any;
  GherLengthTypeNm:any;
  DuppattaLengthNm:any;
  constructor(public router: Router,private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder, private apiServices: ApiService,
public tools: Tools) { 

  this.page = this.activatedRoute.snapshot.paramMap.get('page')
  
    this.form = this.formBuilder.group({
      Shoulder: ['',Validators.compose([Validators.required,Validators.maxLength(10), Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')])],
      FrontNeck: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      Back: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      Yoke: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      Hips: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      Chest: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      UpperBust: ['',[Validators.required, Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      MidBust: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      SleevesLength: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      SleevesLengthType: [''],
      WaistSize: ['',[Validators.required, Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      WaistType: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      DressLength: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      DressLengthType: [''],
      TopLength: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      TopLengthType: [''],
      BottomLength: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      BottomLengthType: [''],
      Gher: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      GherLengthType: [''],
      DuppattaType: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      DuppattaLengthType: [''],
      TuchsPoin: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      Crotch: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      Armhole: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      Knee: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      Pants: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      WaistHip: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
      Thighs: ['',[Validators.maxLength(10),Validators.pattern('[0-9]+(\.[0-9][0-9]?)?')]],
    });
  
}
  ngOnInit() {
  //  this.callStateList()
  }
  ionViewDidEnter() {
    this.MeasurementCall();
  }

  MeasurementCall() {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      let postData = new FormData();
      postData.append("CustomerID", this.apiServices.getCustomerData().CustomerID);
      console.log('Post Data ',postData)
      this.apiServices.getMeasurement(postData).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        if(res.status && res.data != undefined){
          this.Measurement = res.data.Measurement;
        }

        if(this.Measurement != undefined){
       
console.log(' FrontNeck',this.Measurement.FrontNeck);
          this.form.get('Shoulder').setValue(this.Measurement.Shoulder);
          this.form.get('FrontNeck').setValue(this.Measurement.FrontNeck);
          this.form.get('Back').setValue(this.Measurement.Back);
          this.form.get('Yoke').setValue(this.Measurement.Yoke);
          this.form.get('Hips').setValue(this.Measurement.Hips);
          this.form.get('Chest').setValue(this.Measurement.Chest);
          this.form.get('UpperBust').setValue(this.Measurement.UpperBust);
          this.form.get('MidBust').setValue(this.Measurement.MidBust);
          this.form.get('SleevesLength').setValue(this.Measurement.SleevesLength);
          this.form.get('SleevesLengthType').setValue(this.Measurement.SleevesLengthType);
          this.form.get('WaistSize').setValue(this.Measurement.WaistSize);
          this.form.get('WaistType').setValue(this.Measurement.WaistType);
          this.form.get('DressLength').setValue(this.Measurement.DressLength);
          this.form.get('DressLengthType').setValue(this.Measurement.DressLengthType);
          this.form.get('TopLength').setValue(this.Measurement.TopLength);
          this.form.get('TopLengthType').setValue(this.Measurement.TopLengthType);
          this.form.get('BottomLength').setValue(this.Measurement.BottomLength);
          this.form.get('BottomLengthType').setValue(this.Measurement.BottomLengthType);
          this.form.get('Gher').setValue(this.Measurement.Gher);
          this.form.get('GherLengthType').setValue(this.Measurement.GherLengthType);
          this.form.get('DuppattaType').setValue(this.Measurement.DuppattaType);
          this.form.get('DuppattaLengthType').setValue(this.Measurement.DuppattaLengthType);
          this.form.get('TuchsPoin').setValue(this.Measurement.TuchsPoin);
          this.form.get('Crotch').setValue(this.Measurement.Crotch);
          this.form.get('Armhole').setValue(this.Measurement.Armhole);
          this.form.get('Knee').setValue(this.Measurement.Knee);
          this.form.get('Pants').setValue(this.Measurement.Pants);
          this.form.get('WaistHip').setValue(this.Measurement.WaistHip);
          this.form.get('Thighs').setValue(this.Measurement.Thighs);


          this.SleevesTypeNm= this.Measurement.SleevesLengthType;
          this.WaitsTypeNm= this.Measurement.WaistType;
          this.DressLengthTypeNm= this.Measurement.DressLengthType;
          this.TopLengthNm= this.Measurement.TopLengthType;
          this.BottomLengthNm= this.Measurement.BottomLengthType;
          this.GherLengthTypeNm= this.Measurement.GherLengthType;
          this.DuppattaLengthNm= this.Measurement.DuppattaLengthType;

          
        }

        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);

      });
    } else {
      this.tools.closeLoader();
    }
  }  

  onSubmit(){
          
    let  Shoulder= this.form.get('Shoulder').value;
    let  FrontNeck= this.form.get('FrontNeck').value;
    let  Back= this.form.get('Back').value;
    let  Yoke= this.form.get('Yoke').value;
    let  Hips= this.form.get('Hips').value;
    let  Chest= this.form.get('Chest').value;
    let  UpperBust= this.form.get('UpperBust').value;
    let MidBust= this.form.get('MidBust').value;
    let  SleevesLength= this.form.get('SleevesLength').value;
    let  SleevesLengthType= this.form.get('SleevesLengthType').value;
    let  WaistSize= this.form.get('WaistSize').value;
    let  WaistType= this.form.get('WaistType').value;
    let DressLength= this.form.get('DressLength').value;
    let DressLengthType= this.form.get('DressLengthType').value;
    let TopLength= this.form.get('TopLength').value;
    let TopLengthType= this.form.get('TopLengthType').value;
    let BottomLength= this.form.get('BottomLength').value;
    let BottomLengthType= this.form.get('BottomLengthType').value;
    let Gher= this.form.get('Gher').value;
    let GherLengthType= this.form.get('GherLengthType').value;
    let DuppattaType= this.form.get('DuppattaType').value;
    let DuppattaLengthType= this.form.get('DuppattaLengthType').value;
    let TuchsPoin= this.form.get('TuchsPoin').value;
    let Crotch= this.form.get('Crotch').value;
    let Armhole= this.form.get('Armhole').value;
    let Knee= this.form.get('Knee').value;
    let Pants= this.form.get('Pants').value;
    let WaistHip= this.form.get('WaistHip').value;
    let Thighs= this.form.get('Thighs').value;


    console.log('Shoulder ==> ',this.form.get('Shoulder').invalid);
    console.log('UpperBust ==> ',UpperBust);
    console.log('WaistSize ==> ',WaistSize);
    var msg = ''
    if (this.form.get('Shoulder').invalid) {
      if (Shoulder == '') {
        msg = msg + 'Please enter Shoulder valu<br />'
      } else {
        msg = msg + 'Please enter Shoulder valid value<br />'
      }
    }
    if (this.form.get('UpperBust').invalid) {
      if (UpperBust == '') {
        msg = msg + 'Please enter Upper Bust valu<br />'
      } else {
        msg = msg + 'Please enter Upper Bust valid value<br />'
      }
    }
    if (this.form.get('WaistSize').invalid) {
      if (WaistSize == '') {
        msg = msg + 'Please enter Waist Size valu<br />'
      } else {
        msg = msg + 'Please enter Waist Size valid value<br />'
      }
    }
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
          if (this.tools.isNetwork()) {
            this.tools.openLoader();
            let postData = new FormData();
            // if( this.Measurement !=undefined && this.Measurement.MeasurementID != undefined){
            //   postData.append("MeasurementID", this.Measurement.MeasurementID);
            // }
            postData.append("CustomerID", this.apiServices.getCustomerData().CustomerID);
            postData.append("Shoulder", Shoulder);
            postData.append("FrontNeck", FrontNeck);
            postData.append("Back", Back);
            postData.append("Yoke", Yoke);
            postData.append("Hips", Hips);
            postData.append("Chest", Chest);
            postData.append("UpperBust", UpperBust);
            postData.append("MidBust", MidBust);
            postData.append("DressLength", DressLength);
            postData.append("DressLengthType",DressLengthType);
            postData.append("TopLength", TopLength);
            postData.append("TopLengthType",TopLengthType);
            postData.append("BottomLength", BottomLength);
            postData.append("BottomLengthType",BottomLengthType);
            postData.append("Gher", Gher);
            postData.append("GherLengthType", GherLengthType);
            postData.append("DuppattaType",DuppattaType);
            postData.append("DuppattaLengthType", DuppattaLengthType);
            postData.append("TuchsPoin", TuchsPoin);
            postData.append("Crotch", Crotch);
            postData.append("Armhole", Armhole);
            postData.append("Knee", Knee);
            postData.append("Pants", Pants);
            postData.append("WaistHip", WaistHip);
            postData.append("Thighs", Thighs);
            postData.append("WaistSize", WaistSize);
            postData.append("WaistType", WaistType);
            postData.append("SleevesLength", SleevesLength);
            postData.append("SleevesLengthType", SleevesLengthType);
            
     
            this.apiServices.addMeasurement(postData).subscribe(response => {
              this.tools.closeLoader();
              let res:any = response;    
              if (res.status) {
               if(this.isShow){
                 console.log('Measurement Add ',res);
                this.router.navigateByUrl('/select-product/'+res.data.Measurement.MeasurementID);
              }else{
                this.tools.presentAlert('', res.message, 'Ok'); 
              }
  
              } else {
               this.tools.presentAlert('', res.message, 'Ok');
             }
            }, (error: Response) => {
              this.tools.closeLoader();
              console.log('Error ', error);
              let err:any = error;
              this.tools.openAlertToken(err.status, err.error.message);
        
            });
          }else{
            this.tools.closeLoader();
          }   
  }}
}
