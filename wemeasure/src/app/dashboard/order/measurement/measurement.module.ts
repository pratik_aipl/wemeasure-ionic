import { NgModule } from '@angular/core';



import { MeasurementPage } from './measurement.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: MeasurementPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MeasurementPage]
})
export class MeasurementPageModule {}
