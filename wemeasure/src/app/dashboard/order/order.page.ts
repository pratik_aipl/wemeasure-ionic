import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
  items: any = [];
  itemsAll: any = [];
  isSearch=false;
  page :any;
  constructor(public router: Router,private navCtrl: NavController,public activatedRoute: ActivatedRoute, private apiServices: ApiService,public tools: Tools) { 
    this.page = this.activatedRoute.snapshot.paramMap.get('page');
    }
  
  ngOnInit() {
  //  this.callStateList()
  }
  ionViewDidEnter() {
    this.getOrder((this.page == 'order' ));
  }

  search(){
   
    this.isSearch= !this.isSearch;
  }
  ionCancel(){
    this.items = [];
    this.items = this.itemsAll;
  }
  async ionChange(evt){
    this.items =await this.itemsAll;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }
    console.log('Search ',searchTerm);
    this.items = this.items.filter(item => {
      if (item.FName || item.LName) {
        return ((item.FName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) || (item.LName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1));
      }
    });
  }
  backPage(){
    // this.router.navigateByUrl('/dashboard', { replaceUrl: true });
    this.navCtrl.navigateRoot('/dashboard', { animated: true, animationDirection: 'forward' });
    // this.navCtrl.pop(); .setRoot(HomePage);
    // this.navCtrl. .setRoot(HomePage);
  }
  getOrder(isShow) {
    
    if (this.tools.isNetwork()) {
      if(!isShow)
      this.tools.openLoader();
          this.apiServices.getOrderCustomer().subscribe(response => {
            this.tools.closeLoader();
            let res: any = response;
            console.log('response ', res.data);
            if(res.status && res.data !=undefined){
              this.items= res.data;
              this.itemsAll= res.data;
            }else{
              this.items= [];
              this.itemsAll= [];
            }
           
          }, (error: Response) => {
            this.tools.closeLoader();
            console.log('Error ', error);
            let err:any = error;
            this.tools.openAlertToken(err.status, err.error.message);
      
          });
        }else{
          this.tools.closeLoader();
        }
  
  }
  openOrder(item){
    this.apiServices.setCustomerID(item.CustomerID);
    this.router.navigateByUrl('/order-details/'+item.CustomerID+'/'+item.FName+' '+item.LName);
  }
}
