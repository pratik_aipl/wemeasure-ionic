import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.page.html',
  styleUrls: ['./addcustomer.page.scss'],
})
export class AddCustomerPage implements OnInit {
  form: FormGroup;
  page: any;
  selCity:any='';
  selCityNm:any;
  selState:any='';
  selStateNm:any;
  selCust: any;
  isShow=true;
  isIOS;

  cities: [];
  stateList:any=[];

  constructor(public router: Router,private activatedRoute: ActivatedRoute,public plt: Platform,
    public formBuilder: FormBuilder, private apiServices: ApiService,public tools: Tools) { 
      this.isIOS = plt.is('ios');
  this.page = this.activatedRoute.snapshot.paramMap.get('page')
  if(this.page=='menu'){
    this.isShow=false;
    this.form = this.formBuilder.group({
      fname: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50),Validators.pattern('[a-zA-Z]+')]],
      lname: ['', [Validators.required, Validators.maxLength(50),Validators.pattern('[a-zA-Z]+')]],
      mobilenumber: ['',[Validators.required, Validators.minLength(10),Validators.maxLength(10),Validators.pattern('[0-9]+')]],
    });
  }else{
    this.stateList =this.apiServices.getStateList();
    this.isShow=true;
    this.selCust=this.apiServices.getCustomerData();   
    this.selState =this.selCust != undefined?this.selCust.stateId:''
    this.selStateNm =this.selCust != undefined?this.selCust.statename:''
    
    if(this.selState != undefined)
    this.onChangeState(this.selState);
    
    this.selCity =this.selCust != undefined?this.selCust.cityid:''
    this.selCityNm =this.selCust != undefined?this.selCust.city:''

    // console.log('Selected Customer 0 ',this.selCust.DeliveryDate);
    this.form = this.formBuilder.group({
     fname: [this.selCust != undefined?this.selCust.FName:'', [Validators.required, Validators.minLength(3), Validators.maxLength(50),Validators.pattern('[a-zA-Z]+')]],
      lname: [this.selCust != undefined?this.selCust.LName:'', [Validators.required, Validators.maxLength(50),Validators.pattern('[a-zA-Z]+')]],
      mobilenumber: [this.selCust != undefined?this.selCust.ContectNo:'' ,[Validators.required, Validators.minLength(10),Validators.maxLength(10),Validators.pattern('[0-9]+')]],
      address: [this.selCust != undefined?this.selCust.Address:'', [Validators.required]],
      state: [this.selCust != undefined?this.selCust.stateId:'', [Validators.required]],
      city: [this.selCust != undefined?this.selCust.cityid:'', [Validators.required]],
      District: [this.selCust != undefined?this.selCust.District:'', [Validators.required]],
      zipcode: [this.selCust != undefined?this.selCust.ZipCode:'',[Validators.required, Validators.minLength(5), Validators.maxLength(6),Validators.pattern('[0-9]+')]],
    });
  }
}

async ionViewDidEnter() {
  this.selCust=this.apiServices.getCustomerData();   
  if(this.isShow)
  await this.tools.callStateList();
}

async loadLists(){
  console.log(' State List ',this.apiServices.getStateList());
   if(this.apiServices.getStateList())
   this.stateList =this.apiServices.getStateList();
   else
   await this.tools.callStateList(false);
 }
 
async onChangeState(stateId: number) {
  // this.cities = [];
  this.selCity =''
  this.selCityNm =''
  if (stateId) {
    this.cities = await this.tools.getCities(this.stateList,stateId);  
  } else {
    this.cities = [];
  }
}

  ngOnInit() {
  //  this.callStateList()
  }
 
  onSubmit(){

    let fname = this.form.get('fname').value;   
    let lname = this.form.get('lname').value;
    let mobilenumber = this.form.get('mobilenumber').value;

    var msg = ''
    if (this.form.get('fname').invalid) {       
      if (fname == '' ) {
        msg = msg + 'Please enter first name<br />'
      }else{
          msg = msg + 'First name should be at least 3 letters long and without any space<br />'
        }
      }
      if (this.form.get('lname').invalid) {       
        if (lname == '' ) {
          msg = msg + 'Please enter last name<br />'
        }else{
            msg = msg + 'Last name should contain letters and without any space<br />'
          }
        }
    
    if (this.form.get('mobilenumber').invalid) {
      if (mobilenumber == '') {
        msg = msg + 'Please enter mobile number<br />'    
      }else{
        msg = msg + 'Please enter valid mobile number<br />'
      }
    }
    let city,state,zipcode,address='',District='';
    if(this.isShow){
        city = this.form.get('city').value;
        state = this.form.get('state').value;
       zipcode = this.form.get('zipcode').value;
       address = this.form.get('address').value;
       District = this.form.get('District').value;
     
  
       if (this.form.get('address').invalid) {       
        msg = msg + 'Please enter address<br />'
        }
      if (this.form.get('zipcode').invalid) {       
      if (zipcode == '' ) {
        msg = msg + 'Please enter pin code<br />'
      }else{
          msg = msg + 'Please enter valid pin code<br />'
        }
      }
      if (state == '') {
        msg = msg + 'Please select state<br />'
      }
      if (this.form.get('city').invalid) {       
          msg = msg + 'Please enter city<br />'
        }       
    }
    
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
    if (this.tools.isNetwork()) {
          this.tools.openLoader();
          let postData = new FormData();
          postData.append("FName", fname);
          postData.append("LName", lname);
          postData.append("ContectNo", mobilenumber);
          if(this.isShow){
          
            if( this.selCust !=undefined && this.selCust.CustomerID != undefined){
              postData.append("CustomerID", this.selCust.CustomerID);
            }
            postData.append("City", city);
            postData.append("StateID", state);
            postData.append("ZipCode", zipcode);
            postData.append("Address", address);
            postData.append("District", District);
 
          }
   
          this.apiServices.addCustomer(postData).subscribe(response => {
            this.tools.closeLoader();
            let res:any = response;    
            if (res.status) {
             if(this.isShow){
               console.log('Customer Add ',res);
              this.apiServices.setCustomerData(res.data.customer)
              this.router.navigateByUrl('/measurement');
            }else{
                this.form.reset();
              // this.tools.presentAlert('', res.message, 'Ok'); 
              this.apiServices.setCustomerData(res.data.customer)
              console.log('setCustomerData ',res.data.customer);
              this.router.navigateByUrl('/customer-details/' + res.data.customer.FName+' '+res.data.customer.LName, { replaceUrl: true });
            }

            } else {
             this.tools.presentAlert('', res.message, 'Ok');
           }
          }, (error: Response) => {
            this.tools.closeLoader();
            this.tools.closeLoader();
            let res:any = error;    
            console.log('Error call ', error);
            this.tools.presentAlert('', res.error.message, 'Ok');
          });
        }else{
          this.tools.closeLoader();
        }   
  }
}
}
