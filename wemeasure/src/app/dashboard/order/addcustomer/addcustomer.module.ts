import { NgModule } from '@angular/core';



import { AddCustomerPage } from './addcustomer.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: AddCustomerPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddCustomerPage]
})
export class AddCustomerPageModule {}
