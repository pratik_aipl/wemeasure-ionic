import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventService } from 'src/shared/EventService';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.page.html',
  styleUrls: ['./customer-details.page.scss'],
})
export class CustomerDetailsPage implements OnInit {

  form: FormGroup;
  OrderData: any;
  selState:any='';
  selStateNm:any;

  selCity:any='';
  selCityNm:any;
  stateList:any=[];
  cities:any=[];


isEdit=false;
  constructor(public router: Router,public formBuilder: FormBuilder, private eventService:EventService, public apiServices: ApiService,public tools: Tools) { 
    this.OrderData = this.apiServices.getOrderData();  
    this.stateList =this.apiServices.getStateList();
    this.selState =this.OrderData != undefined?this.OrderData.stateId:''
    this.selStateNm =this.OrderData != undefined?this.OrderData.statename:''

    
    if(this.selState != undefined)
    this.onChangeState(this.selState);
    
    this.selCity =this.OrderData != undefined?this.OrderData.cityid:''
    this.selCityNm =this.OrderData != undefined?this.OrderData.city:''
        //  this.myDate = this.selCust != undefined ? new Date(this.selCust.DeliveryDate).toISOString(): new Date().toISOString();
    
    this.isEdit = this.apiServices.getFrom() != '2';
    // console.log('Selected Customer 0 ',this.selCust.DeliveryDate);

    this.form = this.formBuilder.group({
      fname: [this.OrderData != undefined?this.OrderData.FName:'', [Validators.required, Validators.minLength(3), Validators.maxLength(50),Validators.pattern('[a-zA-Z]+')]],
       lname: [this.OrderData != undefined?this.OrderData.LName:'', [Validators.required, Validators.maxLength(50),Validators.pattern('[a-zA-Z]+')]],
       mobilenumber: [this.OrderData != undefined?this.OrderData.ContectNo:'' ,[Validators.required, Validators.minLength(10),Validators.maxLength(10),Validators.pattern('[0-9]+')]],
       // email: [this.OrderData != undefined?this.OrderData.FName:'', [Validators.required, Validators.email]],
       address: [this.OrderData != undefined?this.OrderData.Address:'', [Validators.required]],
       state: [this.OrderData != undefined?this.OrderData.stateId:'', [Validators.required]],
       city: [this.OrderData != undefined?this.OrderData.cityid:'', [Validators.required]],
       District: [this.OrderData != undefined?this.OrderData.District:'', [Validators.required]],
       zipcode: [this.OrderData != undefined?this.OrderData.ZipCode:'',[Validators.required, Validators.minLength(5), Validators.maxLength(6),Validators.pattern('[0-9]+')]],
     });  
  }
  
  ngOnInit() {
  //  this.callStateList()
  }
  async ionViewDidEnter() {
    await this.tools.callStateList();
  }

  async onChangeState(stateId: number) {
    // this.cities = [];
    this.selCity =''
    this.selCityNm =''
    if (stateId) {
      this.cities = await this.tools.getCities(this.stateList,stateId);  
    } else {
      this.cities = [];
    }
  }
  onSubmit(){

    let fname = this.form.get('fname').value;   
    let lname = this.form.get('lname').value;
    let mobilenumber = this.form.get('mobilenumber').value;

    var msg = ''
    if (this.form.get('fname').invalid) {       
      if (fname == '' ) {
        msg = msg + 'Please enter first name<br />'
      }else{
          msg = msg + 'First name should contain atleast 3 letter<br />'
        }
      }
      if (this.form.get('lname').invalid) {       
        if (lname == '' ) {
          msg = msg + 'Please enter last name<br />'
        }else{
            msg = msg + 'Last name allow only letter<br />'
          }
        }
    
    if (this.form.get('mobilenumber').invalid) {
      if (mobilenumber == '') {
        msg = msg + 'Please enter mobile number<br />'    
      }else{
        msg = msg + 'Please enter valid mobile number<br />'
      }
    }
    let city,state,zipcode,address='',District='';
       city = this.form.get('city').value;
       state = this.form.get('state').value;
       zipcode = this.form.get('zipcode').value;
       address = this.form.get('address').value;
       District = this.form.get('District').value;
  
       if (this.form.get('address').invalid) {       
        msg = msg + 'Please enter address<br />'
        }
      if (this.form.get('zipcode').invalid) {       
      if (zipcode == '' ) {
        msg = msg + 'Please enter pin code<br />'
      }else{
          msg = msg + 'Please enter valid pin code<br />'
        }
      }
      if (state == '') {
        msg = msg + 'Please select state<br />'
      }
      if (city == '') {       
          msg = msg + 'Please enter city<br />'
        }  
    
    
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
    if (this.tools.isNetwork()) {
          this.tools.openLoader();
          let postData = new FormData();
          postData.append("FName", fname);
          postData.append("LName", lname);
          postData.append("ContectNo", mobilenumber);

          
            if( this.OrderData !=undefined && this.OrderData.CustomerID != undefined){
              postData.append("CustomerID", this.OrderData.CustomerID);
            }
            postData.append("City", city);
            postData.append("StateID", state);
            postData.append("ZipCode", zipcode);
            postData.append("Address", address);
            postData.append("District", District);
        //
          
   
          this.apiServices.addCustomer(postData).subscribe(response => {
            this.tools.closeLoader();
            let res:any = response;    
            if (res.status) {
              this.tools.presentAlert('', res.message, 'Ok'); 
              this.eventService.publishFormRefresh(true);
            } else {
             this.tools.presentAlert('', res.message, 'Ok');
           }
          }, (error: Response) => {
            this.tools.closeLoader();
            this.tools.closeLoader();
            console.log('Error ', error);
            let err:any = error;
            this.tools.openAlertToken(err.status, err.error.message);
      
          });
        }else{
          this.tools.closeLoader();
        }   
  }
}


}
