import { MeasurementPageModule } from './../../measurement/measurement.module';
import { CustomerDetailsPageModule } from './../../../customer/customr-details/customer-details.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';



import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { UpdateOrderComponent } from './update-order-model/update-order-model.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ViewDetailsPage } from './view-details.page';
const routes: Routes = [
  {
    path: "tabs",
    component: ViewDetailsPage,
    children: [
      {
        path: "",
        redirectTo: "tabs/(customer-details:customer-details)",
        pathMatch: "full"
      },
      {
        path: "customer-details",
        children: [
          {
            path: "",
            loadChildren: () => import('../view-details/customer-details/customer-details.module').then( m => m.CustomerDetailsPageModule)
          }
        ]
      },
      {
        path: "measurement-details",
        children: [
          {
            path: "",
            loadChildren: () => import('../view-details/measurement-details/measurement-details.module').then( m => m.MeasurementDetailsPageModule)
          }
        ]
      },
      {
        path: "item-details",
        children: [
          {
            path: "",
            loadChildren: () => import('../view-details/item-details/item-details.module').then( m => m.ItemDetailsPageModule)
          }
        ]
      }
    ]
  },
  {
    path: "",
    redirectTo: "tabs/customer-details",
    pathMatch: "full"
  }
];

@NgModule({
  entryComponents: [UpdateOrderComponent],
  imports: [
    SharedModule,
    ReactiveFormsModule,
    FormsModule   ,
    RouterModule.forChild(routes)
  ],
  declarations: [ViewDetailsPage,UpdateOrderComponent]
})
export class ViewDetailsPageModule {}
