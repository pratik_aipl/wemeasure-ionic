import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { NavController, ModalController, AlertController } from '@ionic/angular';
import { UpdateOrderComponent } from './update-order-model/update-order-model.component';
import { async } from '@angular/core/testing';
import { EventService } from 'src/shared/EventService';

@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.page.html',
  styleUrls: ['./view-details.page.scss'],
})
export class ViewDetailsPage implements OnInit {
  from:any;
  page:any;
  pageTitle:any;
  constructor(public router: Router,private activatedRoute: ActivatedRoute, private eventService:EventService,public modalController: ModalController,  public alertController: AlertController, private navCtrl: NavController, private apiServices: ApiService,public tools: Tools) { 
    this.pageTitle = this.activatedRoute.snapshot.paramMap.get('ordname')  
    this.from = this.activatedRoute.snapshot.paramMap.get('from')  
    this.page = this.activatedRoute.snapshot.paramMap.get('page')  
    this.apiServices.setFrom(this.from);
 console.log('From Page ',this.from);

 this.eventService.pageTitle$.subscribe(data => {
  console.log('Event From call ' + data)
  this.pageTitle = data
});

  }
  
  ngOnInit() {
  //  this.callStateList()
  }
  ionViewDidEnter() {
   // this.getOrder();
  }
  backPage(){    
    var item=this.apiServices.getOrderData();   
    console.log('Order Details 0',item);
    this.apiServices.setOrderData(null);
    console.log('Order Details 1 ',this.apiServices.getOrderData());

    if(this.page == 'order')
    this.router.navigateByUrl('/order-details/'+item.CustomerID+'/'+item.FName+' '+item.LName, { replaceUrl: true });
    else
    this.router.navigateByUrl('/notification');
  }
  async changeStatus(){
        console.log('Select Category Click')
      const modal = await this.modalController.create({
        component: UpdateOrderComponent,
        cssClass: 'modal',
        componentProps: { value: this.from =='0'?'upcoming':'delevered', OderHdrID:this.apiServices.getOrderData().OderHdrID }
      });
      await modal.present();
      await modal.onDidDismiss()
        .then((data) => {
          console.log('Selected Staus--> ' + data.data);
          if (data.data) {
            
            this.updateOrder(data.data);
            // this.answer.answers[this.currquekey].file = data.data;
          }
        });
  }
  updateOrder(staus) {
    if (this.tools.isNetwork()) {
    this.tools.openLoader();
        this.apiServices.updateOrder(this.apiServices.getOrderData().OderHdrID,staus).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;
          console.log('response ', res.data);
          if(res.status)
          this.eventService.publishFormRefresh(true);
          this.tools.presentAlert('', res.message, 'Ok');
        }, (error: Response) => {
          this.tools.closeLoader();
          console.log('Error ', error);
          let err:any = error;
          this.tools.openAlertToken(err.status, err.error.message);
    
        });
      }else{
        this.tools.closeLoader();
      }  
  }

  async deleteOrder(){
    const alert = await this.alertController.create({
      message: 'Are you sure you want to delete Order?',
      buttons: [
          {
              text:  'Cancel',
              role: 'cancel',
              handler: () => {
                
              }
          },
          {
              text:'Remove',
              handler: () => {
                this.removeOrder();
              }
          }
      ], backdropDismiss: true
  });
  return await alert.present();
  }
  removeOrder() {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.removeOrder(this.apiServices.getOrderData().OderHdrID).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        this.tools.presentAlert('', res.message, 'Ok');
        this.eventService.publishFormRefresh(true);
        this.backPage();
        
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }

  tabChange(event) {
  
  }

}
