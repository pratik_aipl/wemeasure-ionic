import { NgModule } from '@angular/core';



import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { MeasurementDetailsPage } from './measurement-details.page';
const routes: Routes = [
  {
    path: '',
    component: MeasurementDetailsPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MeasurementDetailsPage]
})
export class MeasurementDetailsPageModule {}
