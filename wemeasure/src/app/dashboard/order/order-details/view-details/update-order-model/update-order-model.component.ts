import { ApiService } from 'src/app/services/api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, IonSlides } from '@ionic/angular';
import { Tools } from 'src/shared/tools';
import { Router } from '@angular/router';
@Component({
  selector: 'app-update-order-model',
  templateUrl: './update-order-model.component.html',
  styleUrls: ['./update-order-model.component.scss'],
})
export class UpdateOrderComponent implements OnInit {
  type: any;
  OderHdrID: any;
  isShow=false;
  selStatus:any;


  constructor(public navParams: NavParams,public router: Router,private apiService:ApiService, public tools: Tools,public modalCtrl: ModalController) {
    this.type = this.navParams.get('value');
    this.OderHdrID = this.navParams.get('OderHdrID');
    console.log(' ==> ' + this.type)
    console.log(' ==> ' + this.OderHdrID)
    if(this.type === 'upcoming'){
      this.isShow=true;
    }else{
      this.isShow=false;
    }
    
  }

  update(){

    if(this.selStatus !=undefined){
      this.modalCtrl.dismiss(this.selStatus);
    }else{
      this.tools.openAlert('Please select any status.');
    }
    // console.log('Status ',);
  }
  cancel(){
    this.modalCtrl.dismiss('');
  }
  ngOnInit() { 

  }


  dismissModal() {
      this.modalCtrl.dismiss('');
  }



}
