import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { ProductListComponent } from '../../../select-product/product-list/product-list.component';
import { EventService } from 'src/shared/EventService';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.page.html',
  styleUrls: ['./item-details.page.scss'],
})
export class ItemDetailsPage implements OnInit {
  OrderData: any;

  selectedProd = 'Select Product';
  MeasurementID: any;
  isEdit = false;
  selProdusts = [];

  isCamera = false;

  imageFile = [];
  imageDataFile = [];
  imageCamera = [];
  imageDataCamera = [];
  deletedImageID = [];
  deletedProdID = [];
  form: FormGroup;
  Date = '';
  myDate: String = new Date().toISOString();
  options = {
    // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
    // selection of a single image, the plugin will return it.
    maximumImagesCount: 10,
    
    // max width and height to allow the images to be.  Will keep aspect
    // ratio no matter what.  So if both are 800, the returned image
    // will be at most 800 pixels wide and 800 pixels tall.  If the width is
    // 800 and height 0 the image will be 800 pixels wide if the source
    // is at least that wide.
    width: 200,
    // height: 675,
    
    // quality of resized image, defaults to 100
    quality: 25,//int (0-100),

    // output type, defaults to FILE_URIs.
    // available options are 
    // window.imagePicker.OutputType.FILE_URI (0) or 
    // window.imagePicker.OutputType.BASE64_STRING (1)
    outputType: 1
};

  constructor(public router: Router,  private imagePicker: ImagePicker,
    private camera: Camera, public modalController: ModalController, private eventService:EventService,public apiServices: ApiService,
    private tools: Tools, public formBuilder: FormBuilder) {
      this.OrderData = this.apiServices.getOrderData();  

      console.log(' Order Data ',this.OrderData);
      this.Date = this.OrderData.DeliveryDate;
      this.selProdusts=this.OrderData.ProductList;
      this.imageFile=this.OrderData.Image;

    this.form = this.formBuilder.group({
      OrderName: [this.OrderData != undefined?this.OrderData.OrderName:'', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      price: [this.OrderData != undefined?this.OrderData.price:'', [Validators.required, Validators.minLength(1), Validators.maxLength(5), Validators.pattern('[0-9]+')]],
      ProDecription: [this.OrderData != undefined?this.OrderData.ProDecription:'', [Validators.maxLength(200)]],
    });

  }


  ngOnInit() {
  //  this.callStateList()
  }
  ionViewDidEnter() {
  }
  async selectProduct() {
    console.log('Select Category Click')
    const modal = await this.modalController.create({
      component: ProductListComponent,
      cssClass: 'modal',
      componentProps: { value: 'type' }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        console.log('Selected Cat--> ', data.data);
        if (data.data) {
          // this.answer.answers[this.currquekey].file = data.data;
          if (data.data == 'new') {
            this.apiServices.setProductData(null);
            this.router.navigateByUrl('/add-product');        
          } else {
            for (let index = 0; index < data.data.length; index++) {
              const element = data.data[index];              
              this.selProdusts.push(element)              
            }            
             this.selProdusts = this.selProdusts.filter((v, i, a) => a.findIndex(t => (t.ProductID === v.ProductID)) === i);
          }
        }
      });
    // else{
    //   this.tools.presentLogout('Login require','Login','Cancel');
    // }
  }

  removeImage(type, item) {
    
   
    if (type == 'prod') {
      let index: number = this.selProdusts.indexOf(item);
      if (item.ProductID) {
        this.deletedProdID.push(item.ProductID);
      }
      if (index > -1) {
        console.log('Seleted Image Remove ')
        this.selProdusts.splice(index, 1);
      }

    } else if (type == 'file') {
      let index: number = this.imageFile.indexOf(item);
      let indexDate: number = this.imageDataFile.indexOf(item);
      console.log('Image Remove ')
      console.log('Image Remove ')
      if (index > -1) {
        if (item.OrderDtlID) {
          this.deletedImageID.push(item.OrderDtlID);
        }  
        this.imageFile.splice(index, 1);
      }
      if (indexDate > -1) {
      this.imageDataFile.splice(indexDate, 1);
      }
    } else {
      let index: number = this.imageCamera.indexOf(item);
      if (index > -1) {
        console.log('Seleted Image Remove ')
        this.imageCamera.splice(index, 1);
        this.imageDataCamera.splice(index, 1);
      }
    }

  }

  Next() {
    console.log( 'is Edit 1 ',this.isEdit);
    this.isEdit=true;    
  }

  Update() {
    console.log( 'is Edit 2 ',this.isEdit);

    let OrderName = this.form.get('OrderName').value;
    let ProDecription = this.form.get('ProDecription').value;
    let price = this.form.get('price').value;

    var msg = ''
    if (this.form.get('OrderName').invalid) {
      if (OrderName == '') {
        msg = msg + 'Please enter order name<br />'
      } else {
        msg = msg + 'Order name should contain at least 3 letters <br />'
      }
    }
    if (this.form.get('price').invalid) {
      msg = msg + 'Please enter price <br />'
    }
    if (this.Date == '') {
      msg = msg + 'Please select Date<br />';
    }
    if (this.form.get('ProDecription').invalid) {
      msg = msg + 'Description maximum 200 characters allowed<br />'
    }

    if(this.selProdusts.length == 0 && this.imageDataCamera.length == 0 && this.imageFile.length == 0){
      msg = msg + 'Please select at least one product or upload photo or upload file<br />'
    }

    if (msg != '') {
      this.tools.openAlert(msg);
    } else {

      if (this.tools.isNetwork()) {
        this.tools.openLoader();
        // Replace extension according to your media type
        // call method that creates a blob from dataUri
        let postData = new FormData();
        // postData.append('file', imageFile);
        postData.append("OderHdrID", this.apiServices.getOrderData().OderHdrID);

        postData.append("CustomerID", this.apiServices.getOrderData().CustomerID);
        postData.append("OrderName", OrderName);
        postData.append("price", price);

        
        postData.append("DeliveryDate", this.Date);

        console.log('Selected Date ',this.Date);
        postData.append("ProDecription", ProDecription);

        postData.append("MeasurementID", this.OrderData.MeasurementID);

        if (this.selProdusts.length > 0)
          postData.append("ProductID", Array.prototype.map.call(this.selProdusts, function (item) { return item.ProductID; }).join(","));

          if(this.deletedImageID.length>0){
            postData.append("DeleteImage", Array.prototype.map.call(this.deletedImageID, function (item) { return item; }).join(","));
          }

          // if(this.deletedProdID.length>0){
          //   postData.append("DeletedProduct", Array.prototype.map.call(this.deletedProdID, function (item) { return item; }).join(","));
          // }

        if (this.imageDataCamera.length > 0) {
          for (let i = 0; i < this.imageDataCamera.length; i++) {
            // const element = array[i];
            const date = new Date().valueOf();
            const imageName = date + '.jpeg';
            var imageBlob: any;
            if (this.imageDataCamera.length > 0){
              imageBlob = this.apiServices.dataURItoBlob(this.imageDataCamera[i].Image);
            postData.append('Image[]', imageBlob, imageName);
            }
          }
        }
        if (this.imageDataFile.length > 0) {
          for (let i = 0; i < this.imageDataFile.length; i++) {
            const date = new Date().valueOf();
            const imageName = date + '.jpeg';
            var imageBlob: any;
            if (this.imageDataFile.length > 0){
              imageBlob = this.apiServices.dataURItoBlob(this.imageDataFile[i].Image);
            postData.append('Image[]', imageBlob, imageName);
            }
          }
        }
        this.apiServices.addOrder(postData).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;

          console.log('Update Order Respone ',res);

          this.tools.presentAlert('', res.message, 'Ok');
          if(res.status){            
                        this.imageCamera= [];
                        this.imageDataCamera= [];

                        this.OrderData = res.data;  

      this.selProdusts=this.OrderData.ProductList;
      this.imageFile=this.OrderData.Image;

      this.form.get('OrderName').setValue(this.OrderData != undefined?this.OrderData.OrderName:'');
      this.form.get('price').setValue(this.OrderData != undefined?this.OrderData.price:'');
      this.form.get('ProDecription').setValue(this.OrderData != undefined?this.OrderData.ProDecription:'');
              this.eventService.publishPageTitle(OrderName);
                        this.eventService.publishFormRefresh(true);
          }

        }, (error: Response) => {
          this.tools.closeLoader();
          this.tools.closeLoader();
          console.log('Error ', error);
          let err:any = error;
          this.tools.openAlertToken(err.status, err.error.message);
    
        });
      } else {
        this.tools.closeLoader();
      }

    }

    this.isEdit=false;    
  }

  UploadPhoto() {
    this.isCamera = true;
    this.pickImage(this.camera.PictureSourceType.CAMERA);
  }
  // UploadFile() {
  //   this.isCamera = false;
  //   this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
  // }
  UploadFile() {
    this.isCamera = false;
    // this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
    this.imagePicker.getPictures(this.options).then((results) => {

      for (var i = 0; i < results.length; i++) {
        let rand = Math.floor(Math.random()*20)+1;
        // this.imageDataFile.push(results[i]);
        //   this.imageFile.push('data:image/png;base64,' + results[i]);               
          this.imageDataFile.push({OrderDtlID:rand,Image:results[i]});
          this.imageFile.push({OrderDtlID:rand,Image:'data:image/png;base64,' + results[i]});
      }
    }, (err) => {
      console.log(' Gallary Pick Error ',err)
     });
  }

    pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      let rand = Math.floor(Math.random()*20)+1;
      if (this.isCamera) {
        // this.imageDataCamera.push(imageData);
        this.imageDataCamera.push({OrderDtlID:rand,Image:imageData});
        this.imageCamera.push({OrderDtlID:rand,Image:'data:image/png;base64,' + imageData});
      } else {
        // this.imageDataFile.push(imageData);
        this.imageDataFile.push({OrderDtlID:rand,Image:imageData});
        this.imageFile.push({OrderDtlID:rand,Image:'data:image/png;base64,' + imageData});
      }
    }, (err) => {
      // Handle error
    });
  }
}
