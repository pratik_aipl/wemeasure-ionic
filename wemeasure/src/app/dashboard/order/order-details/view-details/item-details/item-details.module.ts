import { NgModule } from '@angular/core';



import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { ItemDetailsPage } from './item-details.page';
import { FormsModule } from '@angular/forms';
const routes: Routes = [
  {
    path: '',
    component: ItemDetailsPage
  }
];

@NgModule({
  
  imports: [
    SharedModule,  FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ItemDetailsPage]
})
export class ItemDetailsPageModule {}
