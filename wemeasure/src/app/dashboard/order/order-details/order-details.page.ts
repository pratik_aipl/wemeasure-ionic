import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {
  items: any = [];
  itemsAll: any = [];
  isSearch=false;
  custId:any;
  pageTitle:any;
  constructor(public router: Router,private activatedRoute: ActivatedRoute, private navCtrl: NavController, private apiServices: ApiService,public tools: Tools) { 
    this.custId = this.activatedRoute.snapshot.paramMap.get('custId')  
    this.pageTitle = this.activatedRoute.snapshot.paramMap.get('name')  

  }
  
  ngOnInit() {
  //  this.callStateList()
  }
  ionViewDidEnter() {
   // this.getOrder();
  }
  backPage(){    
    this.apiServices.setOrderData(null);
    this.navCtrl.navigateBack("/order/detail", { replaceUrl: true });
  }
  search(){   
    this.isSearch= !this.isSearch;
  }
  ionCancel(){
    this.items = [];
    this.items = this.itemsAll;
  }
  async ionChange(evt){
    this.items =await this.itemsAll;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }
    console.log('Search ',searchTerm);
    this.items = this.items.filter(item => {
      if (item.FName || item.statename) {
        return ((item.FName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) || (item.statename.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1));
      }
    });
  }
  tabChange(event) {
  
  }

}
