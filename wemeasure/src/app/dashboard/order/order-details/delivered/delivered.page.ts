import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { EventService } from 'src/shared/EventService';
@Component({
  selector: 'app-delivered',
  templateUrl: './delivered.page.html',
  styleUrls: ['./delivered.page.scss'],
})
export class DeliveredPage implements OnInit {
  items: any = [];
  itemsAll: any = [];
  isSearch=false;
  custId:any;
  pageTitle:any;
  constructor(public router: Router,private activatedRoute: ActivatedRoute, private eventService:EventService, private apiServices: ApiService,public tools: Tools) { 
    this.custId = this.apiServices.getCustomerID();  
    this.eventService.formRefreshSource$.subscribe(data => {
      console.log('Event From call ' + data)
      this.getOrder(false);
    });
  }
  
  ngOnInit() {
  //  this.callStateList()
  }
  ionViewDidEnter() {
    this.getOrder(true);
  }
  UpdateStatus(item){
    this.apiServices.setOrderData(item);
    this.router.navigateByUrl('/view-details/'+item.OrderName+'/2'+'/order');
  }
 
  getOrder(isShow) {
    
    if (this.tools.isNetwork()) {
      if(isShow)
      this.tools.openLoader();
          this.apiServices.getOrder(this.custId,'delivered').subscribe(response => {
            this.tools.closeLoader();
            let res: any = response;
            console.log('response ', res.data);
            if(res.status && res.data !=undefined){
              this.items= res.data;
              this.itemsAll= res.data;
            }else{
              this.items= [];
              this.itemsAll= [];
            }
           
          }, (error: Response) => {
            this.tools.closeLoader();
            console.log('Error ', error);
          });
        }else{
          this.tools.closeLoader();
        }
  
  }

}
