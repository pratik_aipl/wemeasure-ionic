import { NgModule } from '@angular/core';



import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { DeliveredPage } from './delivered.page';
const routes: Routes = [
  {
    path: '',
    component: DeliveredPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DeliveredPage]
})
export class DeliveredPageModule {}
