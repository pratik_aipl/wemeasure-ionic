import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';



import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { OrderDetailsPage } from './order-details.page';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
const routes: Routes = [
  {
    path: "tabs",
    component: OrderDetailsPage,
    children: [
      {
        path: "",
        redirectTo: "tabs/(upcomming:upcomming)",
        pathMatch: "full"
      },
      {
        path: "upcomming",
        children: [
          {
            path: "",
            loadChildren: () => import('../order-details/upcoming/upcoming.module').then( m => m.UpComingPageModule)
          }
        ]
      },
      {
        path: "inprogress",
        children: [
          {
            path: "",
            loadChildren: () => import('../order-details/inprogress/inprogress.module').then( m => m.InProgressPageModule)
          }
        ]
      },
      {
        path: "delivered",
        children: [
          {
            path: "",
            loadChildren: () => import('../order-details/delivered/delivered.module').then( m => m.DeliveredPageModule)
          }
        ]
      }
    ]
  },
  {
    path: "",
    redirectTo: "tabs/upcomming",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    SharedModule,
    ReactiveFormsModule,
    FormsModule   ,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderDetailsPage]
})
export class OrderDetailsPageModule {}
