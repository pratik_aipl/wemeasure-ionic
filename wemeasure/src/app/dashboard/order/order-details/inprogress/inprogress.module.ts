import { NgModule } from '@angular/core';



import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { InProgressPage } from './inprogress.page';
const routes: Routes = [
  {
    path: '',
    component: InProgressPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InProgressPage]
})
export class InProgressPageModule {}
