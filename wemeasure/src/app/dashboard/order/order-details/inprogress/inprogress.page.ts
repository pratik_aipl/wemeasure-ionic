import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { ModalController } from '@ionic/angular';
import { EventService } from 'src/shared/EventService';

@Component({
  selector: 'app-inprogress',
  templateUrl: './inprogress.page.html',
  styleUrls: ['./inprogress.page.scss'],
})
export class InProgressPage implements OnInit {
  items: any = [];
  itemsAll: any = [];
  isSearch=false;
  custId:any;
  pageTitle:any;
  constructor(public router: Router, private apiServices: ApiService,private eventService:EventService,public tools: Tools) { 
    this.custId = this.apiServices.getCustomerID();  
    this.eventService.formRefreshSource$.subscribe(data => {
      console.log('Event From call ' + data)
      this.getOrder(false);
    });
  }
  
  ngOnInit() {
  //  this.callStateList()
  }
  ionViewDidEnter() {
    this.getOrder(true);
  }

  UpdateStatus(item){
    this.apiServices.setOrderData(item);
    this.router.navigateByUrl('/view-details/'+item.OrderName+'/1'+'/order');
  }

  getOrder(isShow) {    
    if (this.tools.isNetwork()) {
      if(isShow)
      this.tools.openLoader();
          this.apiServices.getOrder(this.custId,'inprogress').subscribe(response => {
            this.tools.closeLoader();
            let res: any = response;
            console.log('response ', res.data);
            if(res.status && res.data !=undefined){
              this.items= res.data;
              this.itemsAll= res.data;
            }else{
              this.items= [];
              this.itemsAll= [];
            }
           
          }, (error: Response) => {
            this.tools.closeLoader();
            console.log('Error ', error);
            let err:any = error;
            this.tools.openAlertToken(err.status, err.error.message);
      
          });
        }else{
          this.tools.closeLoader();
        }
  
  }

}
