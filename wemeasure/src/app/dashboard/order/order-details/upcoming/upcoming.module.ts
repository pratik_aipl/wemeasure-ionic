import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { UpComingPage } from './upcoming.page';
const routes: Routes = [
  {
    path: '',
    component: UpComingPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UpComingPage]
})
export class UpComingPageModule {}
