import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { ModalController } from '@ionic/angular';
import { EventService } from 'src/shared/EventService';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.page.html',
  styleUrls: ['./upcoming.page.scss'],
})
export class UpComingPage implements OnInit {
  items: any = [];
  custId:any;
  pageTitle:any;
  constructor(public router: Router,  public modalController: ModalController, private eventService:EventService,private apiServices: ApiService,public tools: Tools) { 
    this.custId = this.apiServices.getCustomerID();  

    this.eventService.formRefreshSource$.subscribe(data => {
        console.log('Event From call ' + data)
        this.getOrder(false);
      });
  }
  
  ngOnInit() {
  //  this.callStateList()
  }
  ionViewDidEnter() {
    this.getOrder(true);
  }

 
  // async UpdateStatus(item){
  //     console.log('Select Category Click')
  //     const modal = await this.modalController.create({
  //       component: UpdateOrderComponent,
  //       cssClass: 'modal',
  //       componentProps: { value: 'upcoming', OderHdrID:item.OderHdrID }
  //     });
  //     await modal.present();
  //     await modal.onDidDismiss()
  //       .then((data) => {
  //         console.log('Selected Staus--> ' + data.data);
  //         if (data.data) {
  //           this.updateOrder(item,data.data);
  //           // this.answer.answers[this.currquekey].file = data.data;
  //         }
  //       });
  // }
  UpdateStatus(item){
    this.apiServices.setOrderData(item);
    this.router.navigateByUrl('/view-details/'+item.OrderName+'/0'+'/order');
  }
 
  updateOrder(item,staus) {
    if (this.tools.isNetwork()) {
    this.tools.openLoader();
        this.apiServices.updateOrder(item.OderHdrID,staus).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;
          console.log('response ', res.data);
          this.tools.presentAlert('', res.message, 'Ok');
          if (res.status) {
            let index: number = this.items.indexOf(item);
            if(index > -1){
              this.items.splice(index, 1);
              }
          }         
        }, (error: Response) => {
          this.tools.closeLoader();
          console.log('Error ', error);
          let err:any = error;
          this.tools.openAlertToken(err.status, err.error.message);
    
        });
      }else{
        this.tools.closeLoader();
      }  
}

  getOrder(isShow) {
      if (this.tools.isNetwork()) {
        if(isShow)
      this.tools.openLoader();
          this.apiServices.getOrder(this.custId,'pending').subscribe(response => {
            this.tools.closeLoader();
            let res: any = response;
            console.log('response ', res.data);
            if(res.status && res.data !=undefined){
              this.items= res.data;
            }else{
              this.items= [];
            }           
          }, (error: Response) => {
            this.tools.closeLoader();
            console.log('Error ', error);
            let err:any = error;
            this.tools.openAlertToken(err.status, err.error.message);
      
          });
        }else{
          this.tools.closeLoader();
        }  
  }

}
