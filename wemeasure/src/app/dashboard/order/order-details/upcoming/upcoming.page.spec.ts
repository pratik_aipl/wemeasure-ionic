import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpComingPage } from './upcoming.page';

describe('UpComingPage', () => {
  let component: UpComingPage;
  let fixture: ComponentFixture<UpComingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpComingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpComingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
