import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-addorder',
  templateUrl: './addorder.page.html',
  styleUrls: ['./addorder.page.scss'],
})
export class AddOrderPage implements OnInit {
  customerItems: any = [];
  constructor(public router: Router, private apiServices: ApiService,public tools: Tools) { 
    
    }
    ngOnInit() {
      //  this.callStateList()
      }
    
      ionViewDidEnter() {
        this.CustomerCall();
      }
    
      selectCustomer(item){
        this.apiServices.setCustomerData(item);
        this.router.navigateByUrl('/add-customer/order');
     //   this.router.navigateByUrl('/customer-details/' + item.FName+' '+item.LName);
      }
      CustomerCall() {
        if (this.tools.isNetwork()) {
          this.tools.openLoader();
          this.apiServices.getCustomer().subscribe(response => {
            this.tools.closeLoader();
            let res: any = response;
            if(res.status && res.data !=undefined){
              this.customerItems = res.data.customer_list;
            }else{
              this.customerItems = [];
            }

            console.log(res)
          }, (error: Response) => {
            this.tools.closeLoader();
            this.tools.closeLoader();
            console.log('Error ', error);
            let err:any = error;
            this.tools.openAlertToken(err.status, err.error.message);
      
          });
        } else {
          this.tools.closeLoader();
        }
      }  
  addNewCustomer(){
    this.apiServices.setCustomerData(null);
    this.router.navigateByUrl('/add-customer/order');
  }
  
}
