import { NgModule } from '@angular/core';



import { AddOrderPage } from './addorder.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: AddOrderPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddOrderPage]
})
export class AddOrderPageModule {}
