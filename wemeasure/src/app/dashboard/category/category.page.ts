import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {
  catItems = [];
  catItemsAll = [];
  isSearch=false;
  public progress: number = 0;
  constructor(public router: Router, public formBuilder: FormBuilder, public apiService: ApiService, public tools: Tools) {
  
  }

  ngOnInit() {
    //  this.callStateList()
  }
// Interval function
protected interval: any;

onPress($event) {
    console.log("onPress", $event);
    this.startInterval();
}

onPressUp($event) {
    console.log("onPressUp", $event);
    this.stopInterval();
}

startInterval() {
    const self = this;
    this.interval = setInterval(function () {
        self.progress = self.progress + 1;
    }, 50);
}

stopInterval() {
    clearInterval(this.interval);
}
  search(){
    this.isSearch= !this.isSearch;
  }
  ionCancel(){
    this.catItems = [];
    this.catItems = this.catItemsAll;
  }
  async ionChange(evt){
    this.catItems =await this.catItemsAll;
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      return;
    }
    console.log('Search ',searchTerm);
    this.catItems = this.catItems.filter(item => {
      if (item.Name) {
        return ((item.Name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1));
      }
    });
  }

  category(item) {
    console.log('Category Click ', item)
    this.apiService.setCategoryData(item);
    this.router.navigateByUrl('/add-category/edit');
  }
  addCategory() {
    this.router.navigateByUrl('/add-category/new');
  }
  ionViewDidEnter() {
    this.categoryCall();
  }
  categoryCall() {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiService.getCat().subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;

        if (res.status && res.data !=undefined) {
          this.catItems = res.data.category_list;
          this.catItemsAll = res.data.category_list;
        } else {
          this.catItems = [];
          this.catItemsAll = [];
        }

        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }
}
