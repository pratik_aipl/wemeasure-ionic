import { NgModule } from '@angular/core';



import { AddCategoryPage } from './addcategory.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: AddCategoryPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddCategoryPage]
})
export class AddCategoryPageModule {}
