import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-addcategory',
  templateUrl: './addcategory.page.html',
  styleUrls: ['./addcategory.page.scss'],
})
export class AddCategoryPage implements OnInit {
  form: FormGroup;
  page='';
  title='Add Category'
  Category:any;
  Image:any;


  image;
  imageData;

  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };


  constructor(public router: Router, public formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, 
    private apiServices: ApiService, public tools: Tools, private camera: Camera,public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    private file: File) { 
    // this.items= this.activatedRoute.snapshot.paramMap.get("item")
    // console.log(JSON.parse(this.items))
    this.page = this.activatedRoute.snapshot.paramMap.get('page')  

    if(this.page==='new'){
      this.title='Add Category'
    }else if(this.page==='edit'){     
      this.title='Update Category'
      this.Category=this.apiServices.getCategoryData();
      this.Image = this.Category.Image
    }



      this.form = this.formBuilder.group({
        name: [this.Category !=undefined?this.Category.Name:'', [Validators.required, Validators.minLength(3), Validators.maxLength(50),Validators.pattern('[a-zA-Z_ ]*$')]],
      });
    }
  
  ngOnInit() {

  }
  

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.imageData = imageData;
      this.image = 'data:image/png;base64,'+imageData;
      console.log('Image data ==> '+this.image)
    }, (err) => {
      // Handle error
    });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  onSubmit(){
     let name = this.form.get('name').value;
     var msg = ''
     if (this.form.get('name').invalid) {       
      if (name == '' ) {
        msg = msg + 'Please enter category name<br />'
      }else{
          msg = msg + 'Category name should contain atleast 3 letters<br />'
        }
      }

      if(this.Category == undefined && this.imageData == undefined){
        msg = msg + 'Please select category image<br />'
      }
      if (msg != '') {
        this.tools.openAlert(msg);
      } else {

     if (this.tools.isNetwork()) {
       this.tools.openLoader();
       const date = new Date().valueOf();
       const imageName = date + '.jpeg';
       var imageBlob:any;
       if(this.imageData != undefined)
       imageBlob = this.apiServices.dataURItoBlob(this.imageData);
       let postData = new FormData();
       if(this.Category != undefined)
       postData.append("CategoryID", this.Category.CategoryID);
       postData.append("Name", name);
       if(imageBlob !=undefined)
       postData.append('Image', imageBlob, imageName);

       this.apiServices.addCat(postData).subscribe(response => {
         this.tools.closeLoader();
         let res:any = response;

         if (res.status) {
           this.form.reset();
        //  if(this.Category == undefined)
           this.tools.backPage();
           this.tools.presentAlert('', res.message, 'Ok');
         } else {
          this.tools.presentAlert('', res.message, 'Ok');
        }
       }, (error: Response) => {
         this.tools.closeLoader();
         this.tools.closeLoader();
         console.log('Error ', error);
         let err:any = error;
         this.tools.openAlertToken(err.status, err.error.message);
   
       });
     }else{
       this.tools.closeLoader();
     } }   
  }

  async delete(){
    const alert = await this.alertController.create({
      message: 'All products under this category will be deleted.<br /><br /> Are you sure you want to remove category?',
      buttons: [
          {
              text:  'Cancel',
              role: 'cancel',
              handler: () => {
                
              }
          },
          {
              text:'Remove',
              handler: () => {
                this.deleteCat();
              }
          }
      ], backdropDismiss: true
  });
  return await alert.present();
  }

  deleteCat(){
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      let postData = new FormData();
      if(this.Category != undefined)
      postData.append("CategoryID", this.Category.CategoryID);
      this.apiServices.delCat(postData).subscribe(response => {
        this.tools.closeLoader();
        let res:any = response;
        if (res.status) {          
         this.tools.backPage();
        } else {
         this.tools.presentAlert('', res.message, 'Ok');
       }
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    }else{
      this.tools.closeLoader();
    }    

  }
}
