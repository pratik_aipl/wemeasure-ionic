import { Component } from '@angular/core';
import { MenuController, Platform, AlertController } from '@ionic/angular';
import { Tools } from 'src/shared/tools';
import { Router } from '@angular/router';
import { EventService } from 'src/shared/EventService';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.page.html',
  styleUrls: ['dashboard.page.scss'],
})
export class DashboardPage {
  isFrom: any = false;
  isEdit: any = false;
  isPrint: any = false;
  isShow=true;
  isHide=false;
  notItems = [];
  catItems = [];
  prodItems = [];
  NewProcut = [];
  wishlist = [];
  userImage: any='' ;
  username: any = '';
  mobileno: any = '';
  isIOS: boolean;
  constructor(private menu: MenuController, public apiService: ApiService,private alertController:AlertController,
    public tools: Tools, private eventService: EventService,
    private router: Router, public plt: Platform) {
    this.isIOS = plt.is('ios');
    this.username = apiService.getUserData().firstname + " " + apiService.getUserData().lastname;
    this.mobileno = apiService.getUserData().mobileno;
    this.userImage = apiService.getUserData().Image;
    this.eventService.formNew$.subscribe(data => {
      this.isFrom = !data;
    });

    this.eventService.formRefreshSource$.subscribe(data => {
      this.getNotification(false);
    });
  }
  categoryClick(item) {
    // this.apiService.setCategoryData(item);
    // this.router.navigateByUrl('/add-category/edit');
    this.menu.close();
    this.router.navigateByUrl('/product/home/'+item.Name+'/'+item.CategoryID);
  }
  addCategory() {
    this.router.navigateByUrl('/add-category/new');
  }
  Order() {
    this.menu.close();
    this.router.navigateByUrl('/order/menu');
  }
  WishList() {
    this.menu.close();
    this.router.navigateByUrl('/wish-list');
  }
  Category() {
    this.menu.close();
    this.router.navigateByUrl('/category');
  }
  Product() {
    this.menu.close();
    this.router.navigateByUrl('/product/menu/home/0');
  }
  Customer() {
    this.menu.close();
    this.router.navigateByUrl('/customer');
  }
  AddOrder() {
    this.menu.close();
    this.router.navigateByUrl('/add-order');
  }
  AddProduct() {
    this.menu.close();
    this.apiService.setProductData(null);
    this.router.navigateByUrl('/add-product');
  }
  AddCustomer() {
    this.menu.close();
    this.router.navigateByUrl('/add-customer/menu');
  }
  ChangePassword() {
    this.menu.close();
    this.router.navigateByUrl('/change-password');
  }

  openFirst() {
    this.menu.enable(true, 'first');  // replace with MenuA for your case
    this.menu.open('first');
  }
  openNotification() {
    this.menu.close();
    this.router.navigateByUrl('/notification');
  }
  logout() {
    this.menu.close();
    
    this.presentLogout('Are you sure you want to logout?', 'Logout', 'Cancel')
  }

  async presentLogout(message, btnYes, btnNo) {
    const alert = await this.alertController.create({
        message: message,
        buttons: [
            {
                text: btnNo ? btnNo : 'Cancel',
                role: 'cancel',
                handler: () => {

                }
            },
            {
                text: btnYes ? btnYes : 'Yes',
                handler: () => {
                    this.callLogout(true);
                }
            }
        ], backdropDismiss: false
    });
    return await alert.present();
}

  ionViewDidEnter() {
    this.getNotification(false);
    this.categoryCall();
    this.apiService.userData(this.isShow);
    if(this.isShow){
      this.isShow = !this.isShow;
    }
  }

  callLogout(isShow) {
    
    if (this.tools.isNetwork()) {
      if(isShow)
      this.tools.openLoader();
          this.apiService.logout().subscribe(response => {
            this.tools.closeLoader();
            let res: any = response;
            console.log('response ', res);
           if(res.status){
             localStorage.setItem('we-user','');
             localStorage.setItem('we-token','');
            //  localStorage.clear();
             this.router.navigateByUrl('/login', { replaceUrl: true });
           }else{
             this.tools.presentAlert('',res.message,'Ok');
           }

          }, (error: Response) => {
            this.tools.closeLoader();
            console.log('Error ', error);
          });
        }else{
          this.tools.closeLoader();
        }
  
  }
  getNotification(isShow) {
    
    if (this.tools.isNetwork()) {
      if(isShow)
      this.tools.openLoader();
          this.apiService.getNotification().subscribe(response => {
            this.tools.closeLoader();
            let res: any = response;
            console.log('response ', res.data);
            if(res.status && res.data !=undefined){
              this.notItems = res.data.Notification;
            }else{
              this.notItems =[];
            }
            console.log('Length Of Notification ', this.notItems.length);
          }, (error: Response) => {
            this.tools.closeLoader();
            console.log('Error ', error);
          });
        }else{
          this.tools.closeLoader();
        }
  
  }
  getArrive() {
    if (this.tools.isNetwork()) {
      this.apiService.getArrive().subscribe(response => {
        let res: any = response;
        if (res.status && res.data != undefined) {
          this.NewProcut = res.data.product_list;
        } else {
          this.NewProcut = [];
        }
        this.getFavProductCall();
      }, (error: Response) => {
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
      });
    } else {
      this.tools.closeLoader();
    }
  }
  getWishlist() {
    if (this.tools.isNetwork()) {
      this.apiService.getWishProduct().subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        if (res.status && res.data != undefined) {
          this.wishlist = res.data.product_list;
        } else {
          this.wishlist = [];
        }       
      }, (error: Response) => {
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
      });
    } else {
      this.tools.closeLoader();
    }
  }

  openProfile() {
    this.router.navigateByUrl('/profile');
  }
  viewProduct(item) {
    this.apiService.setProductData(item);
    this.router.navigateByUrl('/product-details');
  }
  categoryCall() {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiService.getCat().subscribe(response => {
        let res: any = response;
        if (res.status && res.data != undefined) {
          this.catItems = res.data.category_list;
        } else {
          this.catItems = [];
        }
      this.getArrive();
    
      }, (error: Response) => {
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
      });
    } else {
      this.tools.closeLoader();
    }
  }

  getFavProductCall() {
    if (this.tools.isNetwork()) {
      this.apiService.getFavProduct().subscribe(response => {
        let res: any = response;
        if (res.status && res.data != undefined) {
          this.prodItems = res.data.product_list;
        } else {
          this.prodItems = [];
        }
        this.getWishlist();
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
      });
    } else {
      this.tools.closeLoader();
    }
  }

  fav(item, status) {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiService.productLike(item.ProductID, status).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        this.tools.presentAlert('', res.message, 'Ok');
        if (res.status) {
          let index: number = this.prodItems.indexOf(item);
          if (index > -1) {
            this.prodItems.splice(index, 1);
          }
        }
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        let err: any = error;
        console.log('Error ', err.error);
      });
    } else {
      this.tools.closeLoader();
    }
  }
  wish(item, status) {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiService.productFav(item.ProductID, status).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        this.tools.presentAlert('', res.message, 'Ok');
        if (res.status) {
          let index: number = this.wishlist.indexOf(item);
          if (index > -1) {
            this.wishlist.splice(index, 1);
          }
        }
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        let err: any = error;
        console.log('Error ', err.error);
      });
    } else {
      this.tools.closeLoader();
    }
  }
}
