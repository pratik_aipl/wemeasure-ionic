import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { EventService } from 'src/shared/EventService';
import { Platform, NavController } from '@ionic/angular';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
  isEdit: any = false;
  isNew: any = false;
  isShow: any = false;
  isIOS:boolean=false;
  items: any = [];
  itemsAll: any = [];
  maxDate: String = new Date().toISOString();
  minDate: String = new Date().toISOString();

  constructor(public router: Router, private navCtrl: NavController,public formBuilder: FormBuilder, private eventService: EventService,
    private apiServices: ApiService, public tools: Tools,public plt: Platform) {
     this.isIOS = plt.is('ios');
   // this.isIOS = false;
    console.log('iOS --> ', this.isIOS);
    

    this.eventService.formRefreshSource$.subscribe(data => {
      //do something here
      console.log('Event call ' + data)
     // this.getNotification(false);
    });
  }
  ionViewDidEnter() {
    this.getNotification(true);
  }

  getNotification(isShow) {
    
    if (this.tools.isNetwork()) {
      if(isShow)
      this.tools.openLoader();
          this.apiServices.getNotification().subscribe(response => {
            this.tools.closeLoader();
            let res: any = response;
            console.log('response ', res.data);
            if(res.status && res.data !=undefined){
              this.items= res.data.Notification;
              this.itemsAll= res.data.Notification;
            }else{
              this.items= [];
              this.itemsAll= [];
            }
           
          }, (error: Response) => {
            this.tools.closeLoader();
            console.log('Error ', error);
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);

          });
        }else{
          this.tools.closeLoader();
        }
  
  }
  ngOnInit() {

  }
  backPage(){
    this.navCtrl.navigateRoot('/dashboard', { animated: true, animationDirection: 'forward' });
  }
viewOrder(item){
  this.apiServices.setOrderData(item);
  var status = '0';
  if(item.Status == 'pending'){
    status = '0'
  }else  if(item.Status == 'inprogress'){
    status = '1'
  }else{
    status = '2'
  }
  this.router.navigateByUrl('/view-details/'+item.OrderName+'/'+status+'/noti');
}
  
}
