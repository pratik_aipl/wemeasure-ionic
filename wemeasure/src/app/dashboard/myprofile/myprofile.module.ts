import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyprofilePage } from './myprofile.page';
import { SharedModule } from 'src/shared/shared.module';
const routes: Routes = [
  {
    path: '',
    component: MyprofilePage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MyprofilePage]
})
export class MyprofilePageModule {}
