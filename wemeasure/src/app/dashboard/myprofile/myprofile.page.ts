import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { Tools } from 'src/shared/tools';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.page.html',
  styleUrls: ['./myprofile.page.scss'],
})
export class MyprofilePage implements OnInit {

  profileForm:FormGroup;
  user:any;
  selState:any='';
  selStateNm:any;
  selCity:any='';
  selCityNm:any;

  isEdit=false;
  isShow=false;
  image;
  imageData;
  stateList = [];
  cities:any=[];

  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };


  constructor(public router: Router,
    public formBuilder: FormBuilder,public apiServices:ApiService,public tools: Tools,private camera: Camera,
    public actionSheetController: ActionSheetController) {
      this.user=apiServices.getUserData();
      this.stateList =this.apiServices.getStateList();
      this.setData();
     }
     async ionViewDidEnter() {
      await this.tools.callStateList();
    }
    edit(){
      this.isEdit=true;
    }
    async loadLists(){
      console.log(' State List ',this.apiServices.getStateList());
       if(this.apiServices.getStateList())
       this.stateList =this.apiServices.getStateList();
       else
       await this.tools.callStateList(false);
     }
     
    pickImage(sourceType) {
      const options: CameraOptions = {
        quality: 100,
        sourceType: sourceType,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
        // this.image = (<any>window).Ionic.WebView.convertFileSrc(imageData);
        this.imageData = imageData;
        this.image = 'data:image/png;base64,'+imageData;
      }, () => {
        // Handle error
      });
    }
  
    async selectImage() {
      const actionSheet = await this.actionSheetController.create({
        header: "Select Image source",
        buttons: [{
          text: 'Load from Library',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }
  
  

  onSubmit() {
    let fname = this.profileForm.get('fname').value;
    let lname = this.profileForm.get('lname').value;
    let email = this.profileForm.get('email').value;
    let mobilenumber = this.profileForm.get('mobilenumber').value;
    let brandname = this.profileForm.get('brandname').value;
    let city = this.profileForm.get('city').value;
    let District = this.profileForm.get('District').value;
    let state = this.profileForm.get('state').value;
    let zip = this.profileForm.get('zip').value;
    var msg = ''
    if (this.profileForm.get('fname').invalid) {       
      if (fname == '' ) {
        msg = msg + 'Please enter first name<br />'
      }else{
          msg = msg + 'First name should be at least 3 letters long and without any space<br />'
        }
      }
      if (this.profileForm.get('lname').invalid) {       
        if (lname == '' ) {
          msg = msg + 'Please enter last name<br />'
        }else{
            msg = msg + 'Last name should contain letters and without any space<br />'
          }
        }
    if (email != '') {
      if (this.profileForm.get('email').invalid)     
      msg = msg + 'Please enter valid email<br />'
    }

    if (this.profileForm.get('mobilenumber').invalid) {
      if (mobilenumber == '') {
        msg = msg + 'Please enter mobile number<br />'    
      }else{
        msg = msg + 'Please enter valid mobile number<br />'
      }
    }
    if (this.profileForm.get('brandname').invalid) {       
      if (brandname == '' ) {
        msg = msg + 'Please enter brand name<br />'
      }else{
          msg = msg + 'Brand name should contain atleast 2 letters<br />'
        }
      }
    
      if (state == '') {
        msg = msg + 'Please select state<br />'
      }
      if (city == '') {       
          msg = msg + 'Please enter city<br />'
        }  
    if (this.profileForm.get('zip').invalid) {       
      if (zip == '' ) {
        msg = msg + 'Please enter pin code<br />'
      }else{
          msg = msg + 'Please enter valid pin code<br />'
        }
      }

    
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
      if (this.tools.isNetwork()) {

        const date = new Date().valueOf();
        // Replace extension according to your media type
        const imageName = date + '.jpeg';
        // call method that creates a blob from dataUri
        var imageBlob:any;
        if(this.imageData != undefined)
        imageBlob = this.apiServices.dataURItoBlob(this.imageData);
        // const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' })

        let postData = new FormData();
        // postData.append('file', imageFile);
        postData.append("id", this.user.id);
        postData.append("firstname", fname);
        postData.append("lastname", lname);
        postData.append("email", email);
        postData.append("mobileno", mobilenumber);
        postData.append("zipcode", zip);
        postData.append("storename", brandname);
        postData.append("stateid", state);
        postData.append("city", city);
        postData.append("District", District);
        if(imageBlob !=undefined)
        postData.append('image', imageBlob, imageName);
        // postData.append('file', imageFile);

        this.tools.openLoader();
        this.apiServices.editProfile(postData).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;
          console.log('response ', res.status);
          if (res.status) {
            localStorage.setItem('we-user',JSON.stringify(res.data.user));
            this.isEdit= false;
          this.tools.presentAlert('', res.message, 'Ok');
          } else {
            this.tools.presentAlert('', res.message, 'Ok');
          }
          console.log('response ', res);
         
        }, (error: Response) => {
          this.tools.closeLoader();
          console.log('Error ', error);
          let err:any = error;
          this.tools.openAlertToken(err.status, err.error.message);
    
        });
      } else {
        this.tools.closeLoader();
      }
    }

  }
  ngOnInit() {
  
  }
  async onChangeState(stateId: number) {
    // this.cities = [];
    this.selCity =''
    this.selCityNm =''
    if (stateId) {
      this.cities = await this.tools.getCities(this.stateList,stateId);  
    } else {
      this.cities = [];
    }
  }
   ionViewWillEnter(){
   this.apiServices.userData(false);
  }
  setData(){
    this.selState =this.user != undefined?this.user.stateId:''
    this.selStateNm =this.user != undefined?this.user.statename:''

    
    if(this.selState != undefined)
    this.onChangeState(this.selState);
    
    this.selCity =this.user != undefined?this.user.cityid:''
    this.selCityNm =this.user != undefined?this.user.city:''

  this.profileForm = this.formBuilder.group({
    image: [this.user != undefined?this.user.Image:''],
    fname: [this.user != undefined?this.user.firstname:'',[Validators.required, Validators.minLength(3), Validators.maxLength(50),Validators.pattern('[a-zA-Z]+')]],
     lname: [this.user != undefined?this.user.lastname:'', [Validators.required, Validators.maxLength(50),Validators.pattern('[a-zA-Z]+')]],
     mobilenumber: [this.user != undefined?+this.user.mobileno:'',[Validators.required, Validators.maxLength(10)]],
     delivary: ['',[Validators.required]],
     brandname: [this.user != undefined?this.user.storename:'', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
     email: [this.user != undefined?this.user.email:'',Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')],
     address: [this.user != undefined?this.user.Address:''],
     state: [this.user != undefined?this.user.stateId:'', [Validators.required]],
     city: [this.user != undefined?this.user.city:'', [Validators.required]],
     District: [this.user != undefined?this.user.District:'', [Validators.required]],
     zip: [this.user != undefined?this.user.zipcode:'', [Validators.required, Validators.minLength(5), Validators.maxLength(6),Validators.pattern('[0-9]+')]],
   });

}


}
