import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.page.html',
  styleUrls: ['./wishlist.page.scss'],
})
export class WishListPage implements OnInit {
  prodItems = [];
  prodItemsAll = [];
  isSearch=false;
  pageMsg = 'Data not available';
  constructor(public router: Router,private apiServices: ApiService,public tools: Tools) { 
  
    }
  
  ngOnInit() {
  //  this.callStateList()
  }
  ionViewDidEnter() {
    this.getFavProductCall();
}

search(){
  this.isSearch= !this.isSearch;
}
ionCancel(){
  this.prodItems = [];
  this.prodItems = this.prodItemsAll;
}
async ionChange(evt){
  this.prodItems =await this.prodItemsAll;
  const searchTerm = evt.srcElement.value;
  if (!searchTerm) {
    return;
  }
  console.log('Search ',searchTerm);
  this.prodItems = this.prodItems.filter(item => {
    if (item.ProName) {
      return ((item.ProName.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1));
    }
  });
}

viewProduct(item){
  this.apiServices.setProductData(item);
  this.router.navigateByUrl('/product-details');

}
getFavProductCall() {
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.getWishProduct().subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;
        if(res.data.product_list !=undefined){
          this.prodItems = res.data.product_list;
          this.prodItemsAll = res.data.product_list;
        }else{
          this.pageMsg=res.message
        }
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }

  fav(item,status){
    if (this.tools.isNetwork()) {
      this.tools.openLoader();
      this.apiServices.productFav(item.ProductID,status).subscribe(response => {
        this.tools.closeLoader();
        let res: any = response;

        this.tools.presentAlert('', res.message, 'Ok');
        if (res.status) {
          let index: number = this.prodItems.indexOf(item);
          if(index > -1){
            this.prodItems.splice(index, 1);
            this.prodItemsAll.splice(index, 1);
            }
        }
        console.log(res)
      }, (error: Response) => {
        this.tools.closeLoader();
        this.tools.closeLoader();
        console.log('Error ', error);
        let err:any = error;
        this.tools.openAlertToken(err.status, err.error.message);
  
      });
    } else {
      this.tools.closeLoader();
    }
  }
}
