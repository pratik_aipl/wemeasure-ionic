import { ApiService } from 'src/app/services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Tools } from 'src/shared/tools';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  cpForm: FormGroup;
  constructor(public router: Router,private apiServices:ApiService,
    public formBuilder: FormBuilder,
    public tools: Tools) {
      this.cpForm = this.formBuilder.group({
        old_password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        new_password: ['', Validators.compose([Validators.required, Validators.minLength(6),Validators.pattern('^(?=.*?[A-Za-z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$')])],
        Confirm: ['', Validators.compose([Validators.required, Validators.minLength(6),Validators.pattern('^(?=.*?[A-Za-z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$')])],
      });
     }
  
  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form);

    var msg = ''
    if (this.cpForm.get('old_password').invalid) {
      msg = msg + 'Please enter old password<br />'
  }

    if (this.cpForm.get('new_password').invalid) {
      if (this.cpForm.get('new_password').value == '') {
        msg = msg + 'Please enter password<br />'
      } else {
        msg = msg + 'passwords should be at least 6 characters long and contain at least one number, one letter and one special character<br />'
      }
    }
    if (this.cpForm.get('Confirm').value == '') {
        msg = msg + 'Please enter confirm password<br />'
    }

    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
      if (this.cpForm.get('new_password').value != this.cpForm.get('Confirm').value) {
        this.tools.openAlert('New Password And Confirm Password Dont Match.');
      } else if (this.cpForm.get('new_password').value == this.cpForm.get('old_password').value) {
        this.tools.openAlert('Old Password And New Password Are Similar.');
      } else {
        if (this.tools.isNetwork()) {
          this.tools.openLoader();
          let postData = new FormData();
          // postData.append('file', imageFile);
          postData.append("OldPassword", this.cpForm.get('old_password').value);
          postData.append("NewPassword", this.cpForm.get('new_password').value);
          this.apiServices.ChangePassword(postData).subscribe(response => {
            this.tools.closeLoader();
            var res = response;
            console.log('Response ', response);
            if (res.status) {
              this.cpForm.reset();
            } //else{
            this.tools.presentAlert('', res.message, 'Ok',true);
            // }
          }, (error: Response) => {
            this.tools.closeLoader();
            this.tools.closeLoader();
            console.log('Error ', error);
            let err:any = error;
            this.tools.openAlertToken(err.status, err.error.message);
      
          });
        } else {
          this.tools.closeLoader();
        }
      }
    }
  }
}
