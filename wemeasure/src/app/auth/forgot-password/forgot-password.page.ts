import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  form: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder, private apiServices: ApiService,
    public tools: Tools) {
    this.form = this.formBuilder.group({
      mno: ['' ,[Validators.required, Validators.maxLength(10),Validators.pattern('[0-9]+')]],
    });
  }

  ngOnInit() {
  }
  onSubmit() {

    let mno = this.form.get('mno').value;
    var msg = ''

    if (this.form.get('mno').invalid) {
      if (mno == '') {
        msg = msg + 'Please enter mobile number<br />'
      } else {
        msg = msg + 'Please enter valid mobile number<br />'
      }
    }
   
    if (msg != '') {
      this.tools.openAlert(msg);
    } else {
    if (this.tools.isNetwork()) {
    
    this.tools.openLoader();
    this.apiServices.ForgotPassword(mno).subscribe(response => {
      let res = response;
      this.tools.closeLoader();
      console.log('response ', res);     
        this.tools.presentAlert('', res.message+'<br /> New Password is : '+res.data.password, 'Ok',true);
    }, (error: Response) => {
      this.tools.closeLoader();
      console.log('Error ', error);
      let err:any = error;
      this.tools.openAlertToken(err.status, err.error.message);
    });}else{
      this.tools.closeLoader();
    }
  }
}
}
