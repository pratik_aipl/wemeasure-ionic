import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Tools } from 'src/shared/tools';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  img: any;
  loginForm: FormGroup;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(public router: Router,
    public formBuilder: FormBuilder, public modalController: ModalController, private apiServices: ApiService,
    public tools: Tools) {
    this.loginForm = this.formBuilder.group({
      email: ['',[Validators.required, Validators.maxLength(10),Validators.pattern('[0-9]+')]],
      password: ['', Validators.required],
      //  email: ['pratik.aipl@gmail.com', [Validators.required, Validators.email]],
      //  password: ['Pratik123', Validators.required],
    });
  }

  ngOnInit() {


  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  onSubmit() {
    // this.tools.openLoader();
    let email = this.loginForm.get('email').value;
    let password = this.loginForm.get('password').value;

    var msg = ''

    if (this.loginForm.get('email').invalid) {
      if (email == '') {
        msg = msg + 'Please enter mobile number<br />'
      } else {
        msg = msg + 'Please enter valid mobile number<br />'
      }
    }
    if (password == '') {
      msg = msg + 'Please enter password<br />'
    }

    if (msg != '') {
      this.tools.openAlert(msg);
    } else {

      if (this.tools.isNetwork()) {
        this.tools.openLoader();
        this.apiServices.login(email, password).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;
          console.log('response ', res.login_token);

          if(res.status && res.data.user.activated != '0'){
            localStorage.setItem('we-token', res.login_token);
            localStorage.setItem('we-user', JSON.stringify(res.data.user));
            this.router.navigateByUrl('/dashboard', { replaceUrl: true });
          }else{
            this.tools.presentAlert('','You have successfully registered. Please wait for the admin to approve your request.', 'Ok');
          }
        }, (error: Response) => {
          this.tools.closeLoader();
          this.tools.closeLoader();
          console.log('Error ', error);
          let err:any = error;
          this.tools.openAlertToken(err.status, err.error.message);
    
          // }
          //  this.router.navigateByUrl('/dashboard');
        });
      } else {
        this.tools.closeLoader();
      }
    }

  }
  goToRegister() {
    this.router.navigateByUrl('/register');
  }
  goActiveAccount() {
    this.router.navigateByUrl('/active-account');
  }
  goToForgot() {
    this.router.navigateByUrl('/forgot-password');
  }
}
