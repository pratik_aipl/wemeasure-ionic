import { NotificationPageModule } from './dashboard/notification/notification.module';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/shared/authguard.service';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard',  canActivate: [AuthGuard],
  loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)},
  { path: 'profile',  canActivate: [AuthGuard],
  loadChildren: () => import('./dashboard/myprofile/myprofile.module').then( m => m.MyprofilePageModule)
  },
  { path: 'notification',  canActivate: [AuthGuard],
  loadChildren: () => import('./dashboard/notification/notification.module').then( m => m.NotificationPageModule)
},
  {
    path: 'order/:page', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/order/order.module').then( m => m.OrderPageModule)
  },
  {
    path: 'customer', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/customer/customer.module').then( m => m.CustomerPageModule)
  },
  {
    path: 'customer-details/:name', canActivate: [AuthGuard],
    // path: 'customer-details',
    loadChildren: () => import('./dashboard/customer/customr-details/customer-details.module').then( m => m.CustomerDetailsPageModule)
  },
  {
    path: 'add-order', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/order/addorder/addorder.module').then( m => m.AddOrderPageModule)
  },
  {
    path: 'order-details/:custId/:name', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/order/order-details/order-details.module').then( m => m.OrderDetailsPageModule)
  },
  {
    path: 'view-details/:ordname/:from/:page', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/order/order-details/view-details/view-details.module').then( m => m.ViewDetailsPageModule)
  },
  {
    path: 'add-customer/:page', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/order/addcustomer/addcustomer.module').then( m => m.AddCustomerPageModule)
  },
  {
    path: 'measurement', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/order/measurement/measurement.module').then( m => m.MeasurementPageModule)
  },
  
  {
    path: 'select-product/:measurementID', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/order/select-product/select-product.module').then( m => m.SelectProductPageModule)
  },
  {
    path: 'product/:page/:catName/:catId', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/product/product.module').then( m => m.ProductPageModule)

  },
  {
    path: 'product-details', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/product/product-details/product-details.module').then( m => m.ProductDetailsPageModule)

  },
  {
    path: 'wish-list', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/wishlist/wishlist.module').then( m => m.WishListPageModule)
  },
  {
    path: 'add-product', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/product/addproduct/addproduct.module').then( m => m.AddProductPageModule)
  },
  {
    path: 'category', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/category/category.module').then( m => m.CategoryPageModule)
  },
  {
    path: 'add-category/:page', canActivate: [AuthGuard],
    loadChildren: () => import('./dashboard/category/addcategory/addcategory.module').then( m => m.AddCategoryPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./auth/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'change-password',
    canActivate: [AuthGuard],
    loadChildren: () => import('./change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./auth/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
