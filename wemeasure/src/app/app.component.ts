import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Component, ViewChildren, QueryList } from '@angular/core';

import { Platform, IonRouterOutlet, ModalController, MenuController, ActionSheetController, PopoverController, ToastController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private toast: ToastController,
    private statusBar: StatusBar,
    public modalCtrl: ModalController,
    private router: Router,private oneSignal: OneSignal,
    private menu: MenuController,
    private actionSheetCtrl: ActionSheetController,
    private popoverCtrl: PopoverController,private navCtrl: NavController,
  ) {
    this.initializeApp();
    this.backButtonEvent();
    this.callOneSignal();
  }

  

  callOneSignal() {
    this.oneSignal.startInit('92c863cc-2916-48d7-950b-7a4979f1815e', '669296867239');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    
    
    this.oneSignal.handleNotificationReceived().subscribe(() => {
     // do something when notification is received
    });
    
    this.oneSignal.handleNotificationOpened().subscribe(() => {
      // do something when a notification is opened
    });
    
    this.oneSignal.endInit();
    this.oneSignal.getIds().then((id) => {
      console.log('userId ==> ',id.userId);
      console.log('pushToken ==> ',id.pushToken);
      localStorage.setItem('PlayerID',id.userId);
      
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false); 
      this.statusBar.show();
      this.splashScreen.hide();
      this.callOneSignal();
    });
  }
  // active hardware back button
  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(999999, async () => {
      console.log('subscribeWithPriority Route URL ', this.router.url);
      // navigator['app'].exitApp();

      try {
        const element = await this.actionSheetCtrl.getTop();
        if (element) {
          console.log('Action Sheet');
          element.dismiss();
          return;
        }
      } catch (error) {
      }
  
      // close popover
      try {
        const element = await this.popoverCtrl.getTop();
        if (element) {
          console.log('Popver');
          element.dismiss();
          return;
        }
      } catch (error) {
      }
  
      // close modal
      try {
        const element = await this.modalCtrl.getTop();
        if (element) {
          console.log('Model');
          element.dismiss();
          return;
        }
      } catch (error) {
        
  
      }
  
      // close side menua
      try {
        const element = await this.menu.isOpen();
        console.log('Menu ',element);
        if (element) {
            this.menu.close();
            return;
        }
      } catch (error) {
      }
    
      console.log('Back Page');

      if (this.router.url === '/dashboard'  ) {
        if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
          navigator['app'].exitApp(); // work in ionic 4
        } else {
          this.presentToast('Press back again to exit App.');
          this.lastTimeBackPress = new Date().getTime();
        }
      }
      else if (this.router.url === '/order'  ) {
        this.navCtrl.navigateRoot('/dashboard', { animated: true, animationDirection: 'forward' });
      } 
      else if (this.router.url === '/notification'  ) {
        this.navCtrl.navigateRoot('/dashboard', { animated: true, animationDirection: 'forward' });
      } 
      else if (this.router.url === '/login') {
        navigator['app'].exitApp(); // work in ionic 4
      } else {
        console.log('Back Page else');
        this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
          if (outlet && outlet.canGoBack()) {
            outlet.pop();
          }
        });
      }

    });
    // this.platform.backButton.subscribe(async () => {
    //   console.log('subscribe Route ', this.router.url);
    // });
  }
  async presentToast(msg) {
    const toast = await this.toast.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
}
